<?php 
header('Access-Control-Allow-Origin: *');
session_start();
require_once('support.php');
$index = true;
require_once('db.php');
$shared_list = select("shared_outcome");
$order_list = select("orders");
$order_byid = array();
$month_order = (object)NULL;
$month_value = (object)NULL;
for ($i=0; $i<count($order_list); $i++) {
	$order_list[$i]->total_shared = 0;
	$date = explode("-",$order_list[$i]->start_date);
	$month = $date[0]."-".$date[1];
	if (!isset($month_order->$month)) $month_order->$month = array();
	array_push($month_order->$month, $order_list[$i]->Id);
	$agent_commission = select1("input",'`inout` = 2 AND `payment_code` = "'.$order_list[$i]->code.'"');
	$order_list[$i]->agent_value = $agent_commission->amount;
	$order_byid[$order_list[$i]->Id] = $order_list[$i];
}

foreach ($month_order as $key => $value) {
	$month_value->$key = 0;
	foreach ($value as $key2 => $value2) {
		$month_value->$key += $order_byid[$value2]->total;
	}
}

for ($i=0; $i<count($shared_list); $i++) {
	if ($shared_list[$i]->month<10) $shared_list[$i]->month = '0'.$shared_list[$i]->month;
	$month = $shared_list[$i]->year."-".$shared_list[$i]->month;
	$single = $shared_list[$i]->amount / $month_value->$month;
	foreach ($month_order->$month as $key => $value) {
		$order_byid[$value]->total_shared += floor($single * $order_byid[$value]->total);
	}
}

for ($i=0; $i<count($order_list); $i++) {
	$by = (object)NULL;
	$by->share_value = $order_list[$i]->total_shared;
	$by->order = $order_list[$i]->Id;
	$by->amount = $order_list[$i]->total;
	$by->agent_value = $order_list[$i]->agent_value;
	edit("order_value","`order` = ".$by->order, $by);
}

require_once('support_order_value.php');
echo "success";
?>