<?php 
header('Access-Control-Allow-Origin: *');
session_start();
require_once('support.php');
$index = true;
require_once('db.php');
$depot_out = select("depot","`inout` = 1");
$order_list = select("orders");
$order_byid = array();
for ($i=0; $i<count($order_list); $i++) {
	$order_list[$i]->total_depot = 0;
	$order_byid[$order_list[$i]->Id] = $order_list[$i];
}
for ($i=0; $i<count($depot_out); $i++) {
	$order_byid[$depot_out[$i]->order]->total_depot += $depot_out[$i]->amount;
}

for ($i=0; $i<count($order_list); $i++) {
	if ($order_list[$i]->total_depot>0) {
		$by = (object)NULL;
		$by->depot_value = $order_list[$i]->total_depot;
		$by->order = $order_list[$i]->Id;
		edit("order_value","`order` = ".$by->order, $by);
	}
}

require_once('support_order_value.php');
echo "success";
?>