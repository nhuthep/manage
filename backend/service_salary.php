<?php 
header('Access-Control-Allow-Origin: *');
session_start();
require_once('support.php');
$index = true;
require_once('db.php');
$staff_list = select("staffs");
$staff_byid = array();
$date_value = (object)NULL;

for ($i=0; $i<count($staff_list); $i++) {
	$staff_list[$i]->timesheet_list = select("timesheet","`staff` = ".$staff_list[$i]->Id);
	$staff_list[$i]->total_workinghour = (object)NULL;

	$staff_list[$i]->staff_month = select("staff_month","`staff` = ".$staff_list[$i]->Id);
	$staff_list[$i]->month = (object)NULL;

	for ($j=0; $j<count($staff_list[$i]->timesheet_list); $j++) {
		$date = explode("-", $staff_list[$i]->timesheet_list[$j]->date);
		$month = $date[0].'-'.$date[1];
		if (!isset ($staff_list[$i]->total_workinghour->$month)) $staff_list[$i]->total_workinghour->$month = 0;
		$staff_list[$i]->total_workinghour->$month += $staff_list[$i]->timesheet_list[$j]->total_mins;
	}

	// echo $staff_list[$i]->Id.' ('.$staff_list[$i]->salary.'): <br>';
	// echo "&nbsp;&nbsp;- Working hours by month:<br>";
	// foreach ($staff_list[$i]->total_workinghour as $key => $value) {
	// 	echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$key.': '.$value.'<br>';
	// }
	// echo "";

	for ($j=0; $j<count($staff_list[$i]->staff_month); $j++) {
		if ($staff_list[$i]->staff_month[$j]->month < 10) $staff_list[$i]->staff_month[$j]->month = '0'.$staff_list[$i]->staff_month[$j]->month;
		$month = $staff_list[$i]->staff_month[$j]->year.'-'.$staff_list[$i]->staff_month[$j]->month;
		if ($staff_list[$i]->total_workinghour->$month > 0) {
			$staff_list[$i]->staff_month[$j]->min_salary = $staff_list[$i]->staff_month[$j]->total_salary / $staff_list[$i]->total_workinghour->$month;
			$staff_list[$i]->month->$month = $staff_list[$i]->staff_month[$j];
		}
	}

	// echo '&nbsp;&nbsp;- Salary by minute:<br>';
	// foreach ($staff_list[$i]->staff_month as $key => $value) {
	// 	echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$value->month.': '.$value->min_salary.'<br>';
	// }

	for ($j=0; $j<count($staff_list[$i]->timesheet_list); $j++) {
		$date = $staff_list[$i]->timesheet_list[$j]->date;
		if (!isset($date_value->$date)) $date_value->$date = 0;
		$date_value->$date += $staff_list[$i]->timesheet_list[$j]->total_mins * $staff_list[$i]->month->$month->min_salary;
	}
}

// echo '==================================================================<br>';
// echo '&nbsp;&nbsp;- Date value:<br>';
// foreach ($date_value as $key => $value) {
// 	echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$key.': '.floor($value).'<br>';
// }
// echo '==================================================================<br><br>';

$staff_byid[$staff_list[$i]->Id] = $staff_list[$i];

$daily_task_list = select("daily_tasks");
$daily_task_bydate = (object)NULL;
$date_total_hour = (object)NULL;
$order_total_value = array();

for ($i=0; $i<count($daily_task_list); $i++) {
 	$date = $daily_task_list[$i]->date;
 	if (!isset($daily_task_bydate->$date)) $daily_task_bydate->$date = array();
 	array_push($daily_task_bydate->$date,$daily_task_list[$i]);
 	if (!isset($date_total_hour->$date)) $date_total_hour->$date = 0;
	$date_total_hour->$date += $daily_task_list[$i]->hours;
}

// foreach ($date_total_hour as $key => $value) {
// 	echo $key.': '.$value.'<br>';
// }

for ($i=0; $i<count($daily_task_list); $i++) {
	$date = $daily_task_list[$i]->date;
	$daily_task_list[$i]->value = $daily_task_list[$i]->hours / $date_total_hour->$date * $date_value->$date;
	if (!isset($order_total_value[$daily_task_list[$i]->order])) $order_total_value[$daily_task_list[$i]->order] = 0;
	$order_total_value[$daily_task_list[$i]->order] += $daily_task_list[$i]->value;
}

foreach ($order_total_value as $key => $value) {
	$by = (object)NULL;
	$by->salary_value = $value;
	$by->order = $key;
	edit("order_value","`order` = ".$key, $by);
}

require_once('support_order_value.php');
echo "success";
?>