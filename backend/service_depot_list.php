<?php 
header('Access-Control-Allow-Origin: *');
session_start();
require_once('support.php');
$index = true;
require_once('config.php');
$db->name = $_SESSION["company_code"];
mysql_connect($db->host, $db->user, $db->pass);
mysql_select_db($db->name) or die( "Unable to select database");
$colname = array("Id","depot_type","quantity","amount");
$dataname = "depot";
$query = 'SELECT `Id`,`depot_type`,`quantity`,`amount` FROM `depot` WHERE `inout` = 0 ORDER BY `date`';
$result = mysql_query($query);
$total_in = array();
$total_out = array();

$total_amount_in = array();
$price_in = array();

$amount_in = array();
$amount_out = array();

$quantity_in = array();
$quantity_out = array();

$id_in = array();
$id_out = array();

$total = array();
$total_amount = array();
$total_status = array();
$total_remain = array();
$total_remain_value = array();
$total_time_order = array();
$total_this_amount = array();
$total_id = array();

$str_inout = array("+","-");

if (mysql_num_rows($result)) {
	for ($i=0;$i<mysql_num_rows($result);$i++) {
		$data[$i] = (object)NULL;
		for ($j=0;$j<count($colname);$j++) {
			$data[$i]->$colname[$j] = mysql_result($result, $i, $colname[$j]);
		}
		if ($data[$i]->quantity!=0) {
			if (isset($total_in[$data[$i]->depot_type])) {
				$length = count($total_in[$data[$i]->depot_type]);
				array_push($total_in[$data[$i]->depot_type], $total_in[$data[$i]->depot_type][$length-1]+$data[$i]->quantity);
				array_push($total_amount_in[$data[$i]->depot_type], $total_amount_in[$data[$i]->depot_type][$length-1]+$data[$i]->amount);
			} else {
				$total_in[$data[$i]->depot_type] = array();
				$total_amount_in[$data[$i]->depot_type] = array();
				$price_in[$data[$i]->depot_type] = array();
				$amount_in[$data[$i]->depot_type] = array();
				$quantity_in[$data[$i]->depot_type] = array();
				$id_in[$data[$i]->depot_type] = array();
				$length = 0;
				array_push($total_in[$data[$i]->depot_type], $data[$i]->quantity);
				array_push($total_amount_in[$data[$i]->depot_type], $data[$i]->amount);
			}
			array_push($price_in[$data[$i]->depot_type], $data[$i]->amount/$data[$i]->quantity);
			array_push($amount_in[$data[$i]->depot_type], $data[$i]->amount);
			array_push($quantity_in[$data[$i]->depot_type], $data[$i]->quantity);
			array_push($id_in[$data[$i]->depot_type], $data[$i]->Id);
		}
	}
}

// $total_in[depot_type] is an array which describe the total quantity input of the type at the moment
// $total_amount_in[depot_type] is an array which describe the total value input of the type at the moment
// $total_price

// foreach ($total_in as $key => $value) {
// 	if ($key == 32) {
// 		echo '<br><br>key = '.$key.'<br>{';
// 		for ($j=0; $j<count($value); $j++) {
// 			echo '$total_in['.$key.']['.$j.'] = '.$value[$j].'<br>';
// 			echo '$total_amount_in['.$key.']['.$i.'] = '.$total_amount_in[$key][$j].'<br>';
// 		}
// 		echo '}';
// 	}
// }

$query = 'SELECT `Id`,`depot_type`,`quantity`,`amount` FROM `depot` WHERE `inout` = 1 ORDER BY `date`';
$result = mysql_query($query);
if (mysql_num_rows($result)) {
	for ($i=0;$i<mysql_num_rows($result);$i++) {
		$data[$i] = (object)NULL;
		for ($j=0;$j<count($colname);$j++) {
			$data[$i]->$colname[$j] = mysql_result($result, $i, $colname[$j]);
		}
		if ($data[$i]->quantity!=0) {
			if (isset($total_out[$data[$i]->depot_type])) {
				$length = count($total_out[$data[$i]->depot_type]);
				array_push($total_out[$data[$i]->depot_type], $total_out[$data[$i]->depot_type][$length-1]+$data[$i]->quantity);
			} else {
				$total_out[$data[$i]->depot_type] = array();
				$total_amount_out[$data[$i]->depot_type] = array();
				$amount_out[$data[$i]->depot_type] = array();
				$quantity_out[$data[$i]->depot_type] = array();
				$id_out[$data[$i]->depot_type] = array();
				$length = 0;
				array_push($total_out[$data[$i]->depot_type], $data[$i]->quantity);
			}
			array_push($amount_out[$data[$i]->depot_type], $data[$i]->amount);
			array_push($quantity_out[$data[$i]->depot_type], $data[$i]->quantity);
			array_push($id_out[$data[$i]->depot_type], $data[$i]->Id);
		}
	}
}

// foreach ($total_out as $key => $value) {
// 	if ($key == 32) {
// 		echo '<br><br>key = '.$key.'<br>{';
// 		for ($j=0; $j<count($value); $j++) {
// 			echo '$total_out['.$key.']['.$j.'] = '.$value[$j].'<br>';
// 			echo '$total_amount_out['.$key.']['.$i.'] = '.$total_amount_out[$key][$j].'<br>';
// 		}
// 		echo '}';
// 	}
// }


foreach ($total_in as $key => $total_out_type) {
	$i_in = 1;
	$i_out = 0;

	$total[$key] = array();
	$total_amount[$key] = array();
	$total_status[$key] = array();
	$total_price[$key] = array();
	$total_remain[$key] = array();
	$total_remain_value[$key] = array();
	$total_quantity[$key] = array();
	$total_this_amount[$key] = array();
	$total_time_order[$key] = array();
	$total_id[$key] = array();

	$current_in = $total_in[$key][0];
	$current_amount = $total_amount_in[$key][0];
	$current_price = $price_in[$key][0];
	$current_remain = $total_in[$key][0];
	$current_remain_value = $total_amount_in[$key][0];

	$last_in = 0;
	$last_amount_in = 0;

	$last_out = 0;
	$last_amount_out = 0;
	$last_price = 0;

	$current_total = 0;
	$current_total_price = 0;
	$last_total = 0;
	$last_total_price = 0;
	$last_remain = 0;
	$last_remain_value = 0;

	$i_index = 1;

	array_push($total[$key], $total_in[$key][0]);
	array_push($total_status[$key], 0);
	array_push($total_amount[$key], $total_amount_in[$key][0]);
	array_push($total_price[$key], $price_in[$key][0]);
	array_push($total_remain[$key], $total_in[$key][0]);
	array_push($total_remain_value[$key], $total_amount_in[$key][0]);
	array_push($total_quantity[$key], $quantity_in[$key][0]);
	array_push($total_this_amount[$key], $amount_in[$key][0]);
	array_push($total_time_order[$key], 0);
	array_push($total_id[$key], $id_in[$key][0]);

	$query = 'UPDATE `depot` SET `remain` = '.$current_remain.', `remain_value` = '.$current_remain_value.', `time_order` = 0 WHERE `Id` = '.$id_in[$key][0];
	mysql_query($query);
	// echo $query.'<br />';

	while ($i_in < count($total_in[$key]) || $i_out < count($total_out[$key])) {
		$length = count($total)-1;
		if (($total_out[$key][$i_out] < $current_in || $i_in==(count($total_in))) && $i_out < count($total_out[$key])) {
			if($quantity_out[$key][$i_out]!=0) {
				$last_total = $current_total;
				$last_total_price = $current_total_price;
				$current_total = $total_out[$key][$i_out];
				$current_total_price = $last_amount_in + $current_price*($current_total-$last_in);
				$current_this_amount = $current_total_price-$last_total_price;
				$price = $current_this_amount/($current_total-$last_total);
				$last_remain = $current_remain;
				$current_remain -= $quantity_out[$key][$i_out];
				$last_remain_value = $current_remain_value;
				$current_remain_value = $current_remain_value - $current_this_amount;

				array_push($total[$key], $total_out[$key][$i_out]);
				array_push($total_status[$key],1);
				array_push($total_amount[$key],$current_total_price);
				array_push($total_price[$key],$price);
				array_push($total_quantity[$key], $quantity_out[$key][$i_out]);
				array_push($total_remain[$key],$current_remain);
				array_push($total_remain_value[$key],$current_remain_value);
				array_push($total_this_amount[$key], $current_this_amount);
				array_push($total_time_order[$key],$i_index);
				array_push($total_id[$key],$id_out[$key][$i_out]);
				$query = 'UPDATE `depot` SET `amount`='.$current_this_amount.', `remain` = '.$current_remain.', `remain_value` = '.$current_remain_value.', `time_order` = '.$i_index.' WHERE `Id` = '.$id_out[$key][$i_out];
				mysql_query($query);
				// echo $query.'<br />';
				$i_index++;
			}
			$i_out++;
		} else {
			if($quantity_in[$key][$i_in]!=0) {
				$last_in = $current_in;
				$last_amount_in = $current_amount;
				$last_price = $current_price;

				$current_in = $total_in[$key][$i_in];
				$current_amount = $total_amount_in[$key][$i_in];
				$current_price = $price_in[$key][$i_in];

				$last_remain_value = $current_remain_value;
				$current_remain += $quantity_in[$key][$i_in];
				$current_remain_value = $last_remain_value + $amount_in[$key][$i_in];
				
				array_push($total[$key], $total_in[$key][$i_in]);
				array_push($total_status[$key],0);
				array_push($total_amount[$key], $current_amount);
				array_push($total_price[$key], $price_in[$key][$i_in]);
				array_push($total_quantity[$key], $quantity_in[$key][$i_in]);
				array_push($total_remain[$key],$current_remain);
				array_push($total_remain_value[$key],$current_remain_value);
				array_push($total_this_amount[$key], $amount_in[$key][$i_in]);
				array_push($total_time_order[$key],$i_index);
				array_push($total_id[$key],$id_in[$key][$i_in]);
				$query = 'UPDATE `depot` SET `remain` = '.$current_remain.', `remain_value` = '.$current_remain_value.', `time_order` = '.$i_index.' WHERE `Id` = '.$id_in[$key][$i_in];
				mysql_query($query);
				// echo $query.'<br />';
				
				$i_index++;
			}
			$i_in++;
		}
	}
	$query = 'UPDATE `depot_types` SET `remain` = '.$current_remain.', `remain_value` = '.$current_remain_value.' WHERE `Id` = '.$key;
	mysql_query($query);
	// echo $query.'<br />';
	// echo $key.':<br><table border="1" cell-spacing	="10"><tr><th>+/-</th><th>Id</th><th>Quantity</th><th>Remain</th><th>Amount</th><th>Remain amount</th><th>Price</th><th>Total</th><th>Total amount</th><th>Order</th></tr>';
	// for ($i=0; $i<count($total[$key]); $i++) {
	// 	echo '<tr><td align="center">'.$str_inout[$total_status[$key][$i]].'</td><td align="center">'.$total_id[$key][$i].'</td><td align="right">'.$total_quantity[$key][$i].'</td><td align="right">'.$total_remain[$key][$i].'</td><td align="right">'.floor($total_this_amount[$key][$i]).'</td><td align="right">'.floor($total_remain_value[$key][$i]).'</td><td align="right">'.floor($total_price[$key][$i]).'</td><td align="right">'.$total[$key][$i].'</td><td align="right">'.floor($total_amount[$key][$i]).'</td><td align="center">'.$total_time_order[$key][$i].'</td></tr>';
	// }
	// echo '}</table>';
}

$total_depot_month = (object)NULL;
$colname = array("inout","amount","date");
$query = 'SELECT `inout`,`amount`,`date` FROM `depot` WHERE `Id` != 0 ORDER BY `date`';
$result = mysql_query($query);
if (mysql_num_rows($result)) {
	for ($i=0;$i<mysql_num_rows($result);$i++) {
		$data[$i] = (object)NULL;
		for ($j=0;$j<count($colname);$j++) {
			$data[$i]->$colname[$j] = mysql_result($result, $i, $colname[$j]);
		}

		$tem_date = explode("-", $data[$i]->date);
		$month = $tem_date[0].'-'.$tem_date[1];
		if (!isset($total_depot_month->$month)) $total_depot_month->$month = 0;
		$total_depot_month->$month += (0.5-$data[$i]->inout)*2*$data[$i]->amount;
		// echo $month.':'.$data[$i]->inout.' / '.(0.5-$data[$i]->inout)*2*$data[$i]->amount.'<br>';
	}
}

foreach ($total_depot_month as $key => $value) {
	$by->total_depot_value = $value;
	$tem_date = explode("-", $key);
	$query = 'SELECT `Id` from `total_month` WHERE `year` = "'.$tem_date[0].'" AND `month` = "'.$tem_date[1].'"';
	echo $query;
	$result = mysql_query($query);
	if (mysql_num_rows($result)) {
		$query = 'UPDATE `total_month` SET `total_depot_value` = '.$value.' WHERE `year` = '.$tem_date[0].' AND `month` = '.$tem_date[1];
	} else {
		$query = 'INSERT INTO `total_month` (total_depot_value) VALUES ('.$value.')';
	}
	echo $query;
	mysql_query($query);
	// echo $key.': '.$value.'<br>';
}

mysql_close();
echo "success";
?>