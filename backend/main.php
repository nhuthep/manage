<?php 
header('Access-Control-Allow-Origin: *');
session_start();
$index = true;
require_once('db.php');
$show = NULL;
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$data = json_decode(file_get_contents("php://input"));
	$from = $data->from;
	if (isset($data->id)) {
		if ($data->id == 0) {
			$where = "`Id` = -999";
			unset($data->id);
		} else {
			$where = "`Id` = ".$data->id;
		}
	} else {
		$where = "`Id` = -999";
	}
	$dt = (object)NULL;
	for ($i=0; $i<count($data->describe); $i++) {
		$str = $data->describe[$i];
		$dt->$str = $data->$str;
	}
	if(isset($data->show)) {
		$show = $data->show;
		unset($data->show);
	}
	if(isset($dt->delete)) {
		remove($from, $where, $show);
	} else {
		$id = edit($from, $where, $dt, $show);
		echo $id;
	}
	if(isset($data->version)) {
		file_put_contents('../data/cachedVersion.json', '{"version":"'.$data->version.'"}');
	}
} elseif ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET["agency"])) {
    $data = getAgency();
    echo json_encode($data);
} elseif ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET["from"])) {
	$where = NULL;
	$select = NULL;
	$sort = NULL;
	$asc = NULL;
	$limit = NULL;
	$sorder = NULL;
	$from = $_GET["from"];
	if(isset($_GET["where"])) $where = $_GET["where"];
	if(isset($_GET["select"])) $select = $_GET["select"];
	if(isset($_GET["sort"])) $sort = $_GET["sort"];
	if(isset($_GET["asc"])) $asc = $_GET["asc"];
	if(isset($_GET["limit"])) $where = $_GET["limit"];
	if(isset($_GET["show"])) $show = $_GET["show"];
	if(isset($_GET["sorder"])) $sorder = $_GET["sorder"];
	$data = select($from, $where, $select, $sort, $asc, $limit, $show, $sorder);
	echo json_encode($data);
}
?>