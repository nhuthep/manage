<?php if(!isset($index)) exit();
require_once('config.php');
$db->name = $_SESSION["company_code"];
$noqr = 0;

function select($from, $where = NULL, $select = NULL, $sort = NULL, $asc = NULL, $limit = NULL, $show = NULL, $sorder = NULL) {
	$select = str_replace(' ','', $select);
	$sort = str_replace(' ','', $sort);
	$asc = str_replace(' ','', $asc);
	$data = array();
	if (!$sort) {
		$sort = 'Id';
		$asc = 'a';
	}
	$sort_item = explode(',', $sort);
	$asc_item = array();
	$asc_item = explode(',', $asc);
	if (strtolower($asc_item[0]) == 'desc' || strtolower($asc_item[0]) == 'd') {
		$asc_item[0] = 'DESC';
	} else {
		$asc_item[0] = 'ASC';
	}
	if (!$sorder) $srt = ' ORDER BY `'.$sort_item[0].'` '.$asc_item[0];
	else $srt = ' ORDER BY '.$sort_item[0].' '.$asc_item[0];
	
	for ($i=1; $i<count($sort_item); $i++) {
		if (isset($asc_item[$i])) {
			if (strtolower($asc_item[$i]) == 'desc' || strtolower($asc_item[$i]) == 'd') {
				$asc_item[$i] = 'DESC';
			} else {
				$asc_item[$i] = 'ASC';
			}
		} else {
			$asc_item[$i] = 'ASC';
		}
		$srt .= ', `'.$sort_item[$i].'` '.$asc_item[$i];
	}
	
	if ($select) {
		$allselects = explode(',', $select);
		$sl = "SELECT ";
		for ($i=0; $i<count($allselects); $i++) {
			$sl .= "`".$allselects[$i]."`, ";
		}
		$sl .= "`Id` FROM ";
	} else $sl = "SELECT * FROM ";
	$fr = '`'.$from.'`';
	if (!$where)$where = 'Id != "0"';
	$wh = " WHERE ".$where." AND Id != 0";
	if ($limit) $lm = ' LIMIT '.$limit;
	else $lm = '';
	global $db, $noqr;
	mysql_connect($db->host, $db->user, $db->pass);
	mysql_select_db($db->name) or die( "Unable to select database");
	if (!$select) {
		$column_result = mysql_query("SHOW COLUMNS FROM ".$fr);
		$i=0;
		while ($row = mysql_fetch_row($column_result)) {
			$colname[$i++] = $row[0];
		}
	} else {
		$colname = explode(',', $select);		
	}
	if (in_array('language', $colname) && $_SESSION['language']) $wh.= ' AND `language` = "'.$_SESSION['language'];
	$qr = $sl.$fr.$wh.$srt.$lm;
	$result = mysql_query($qr);
	if (!$result) {
		global $page, $param;
		$file = 'logedit.txt';
		$fh = fopen($file, 'a');
		fwrite($fh, $page.','.$param.','.$qr.';');
		fclose($fh);
	}
	if (mysql_num_rows($result)) {
		for ($i=0;$i<mysql_num_rows($result);$i++) {
			$data[$i] = (object)NULL;
			for ($j=0;$j<count($colname);$j++) {
				$data[$i]->$colname[$j] = mysql_result($result, $i, $colname[$j]);
			}
			$data[$i]->Id = mysql_result($result, $i, "Id");
		}
	}
	if ($show)echo '&qr'.$noqr.'='.$qr.'<br>';
	$noqr++;
	mysql_close();
	return $data;
}

function select1($from, $where = NULL, $select = NULL, $sort = NULL, $asc = NULL, $show = NULL) {
	$select = str_replace(' ','', $select);
	$sort = str_replace(' ','', $sort);
	$asc = str_replace(' ','', $asc);
	$data = (object)NULL;
	
	if ($sort) {
		$datas = select($from, $where, $select, $sort, $asc, '0, 1', $show);
		return $datas[0];
	} else {		
		if ($select) {
			$allselects = explode(',', $select);
			$sl = "SELECT ";
			for ($i=0; $i<count($allselects); $i++) {
				$sl .= "`".$allselects[$i]."`, ";
			}
			$sl .= "`Id` FROM ";
		} else $sl = "SELECT * FROM ";
		$fr = '`'.$from.'`';
		if (!$where)$where = 'Id != "@#$"';
		$wh = " WHERE ".$where." LIMIT 0, 1";
		global $db, $noqr;
		mysql_connect($db->host, $db->user, $db->pass);
		mysql_select_db($db->name) or die( "Unable to select database");
		if (!$select) {
			$column_result = mysql_query("SHOW COLUMNS FROM ".$fr);
			$i = 0;
			while ($row = mysql_fetch_row($column_result)) {
				$colname[$i++] = $row[0];
			}
		} else {
			$colname = explode(',', $select);		
		}
		if (in_array('language', $colname) && $_SESSION['language']) $wh.= ' AND `language` = "'.$_SESSION['language'];
		$qr = $sl.$fr.$wh;
		$result = mysql_query($qr);
		if (mysql_num_rows($result)) {
			$data = (object)NULL;
			for ($j=0;$j<count($colname);$j++) {
				$data->$colname[$j] = mysql_result($result, 0, $colname[$j]);
			}
			$data->Id = mysql_result($result, 0, "Id");
		}
		if ($show)echo '&qr'.$noqr.'='.$qr.'<br>';
		$noqr++;
		mysql_close();
		return $data;
	}
}

function edit($from, $where, $by, $show = NULL) {
	global $db, $noqr;
	$sl = "SELECT * FROM ";
	if ($where !== NULL) {
		$select_data = select($from, $where, NULL, NULL, NULL, NULL, $show);	
		$wh = " WHERE ".$where;
	}
	if (count($select_data)>0 && $where !== NULL) {
		$uf = "UPDATE `".$from."` SET `";
		$i=0;
		foreach($by as $key=>$value) {
			if ($i>0)$uf = $uf.", `";
			$uf = $uf.$key."`='".$value."'";
			$i++;
		}
		$qr = $uf.$wh;
		mysql_connect($db->host, $db->user, $db->pass);
		mysql_select_db($db->name) or die( "Unable to select database");
		mysql_query($qr);
		mysql_close();
		$id = $select_data[0]->Id;
	} else {
		$qr = "INSERT INTO `".$db->name."`.`".$from."` (`";
		$i=0;
		foreach($by as $key=>$value) {
			$bykey[$i] = $key;
			$byvalue[$i] = $value;
			$i++;
		}
		$qr = $qr.$bykey[0];
		for ($i=1;$i<count($bykey);$i++) {
			$qr = $qr."`, `".$bykey[$i];
		}
		$qr = $qr."`) VALUES ('".$byvalue[0];
		for ($i=1;$i<count($byvalue);$i++) {
			$qr = $qr."','".$byvalue[$i];
		}
		$qr = $qr."')";
		mysql_connect($db->host, $db->user, $db->pass);
		mysql_select_db($db->name) or die( "Unable to select database");
		mysql_query($qr);
		$id = mysql_insert_id();
		mysql_close();
		if ($from == 'ws_thiep') {
			global $page, $param;
			$file = 'logthiep.txt';
			$fh = fopen($file, 'a');
			fwrite($fh, $page.','.$param.','.$qr.';');
			fclose($fh);
		}
	}
	if ($show)echo '&qr'.$noqr.'='.$qr.'<br>';
	$noqr++;	
	return $id;
}

function remove($from, $where = NULL, $show = NULL) {
	$fr = "DELETE FROM `".$from."`";
	$wh = '';
	if ($where)$wh = " WHERE ".$where;
	$qr = $fr.$wh;
	global $db, $noqr;
	mysql_connect($db->host, $db->user, $db->pass);
	mysql_select_db($db->name) or die( "Unable to select database");
	mysql_query($qr);
	mysql_close();
	if ($show)echo '&qr'.$noqr.'='.$qr.'<br>';
	$noqr++;
}

function gettables() {
	$data = array();
	$i = 0;
	global $db;
	mysql_connect($db->host, $db->user, $db->pass);
	mysql_select_db($db->name) or die( "Unable to select database");
	$column_result = mysql_query('show tables');
	mysql_close();
	while ($row = mysql_fetch_row($column_result)) {
		$data[$i] = $row[0];
		$i++;
	}
	return $data;
}

function counts($from, $where = NULL, $show = NULL) {
	$sl = 'SELECT COUNT(Id) FROM `';
	$wh = '`';
	if ($where)$wh = '` WHERE '.$where;
	$qr = $sl.$from.$wh;
	global $db, $noqr;
	mysql_connect($db->host, $db->user, $db->pass);
	mysql_select_db($db->name) or die( "Unable to select database");
	$result = mysql_query($qr);
	$row = mysql_fetch_array($result);
	mysql_close();
	if ($show)echo '&qr'.$noqr.'='.$qr.'<br>';
	$noqr++;
	if ($result) return $row['COUNT(Id)']; else return 0;
}

function getAgency() {
    $sql = 'SELECT * FROM partners WHERE type = 1 ';
    global $db;
    mysql_connect($db->host, $db->user, $db->pass);
    mysql_select_db($db->name) or die( "Unable to select database");
    $result = mysql_query($sql);
    mysql_close();

    $i =0;
    while ($row = mysql_fetch_row($result)) {
        $data[$i] = $row;
        $i++;
    }

    return $data;
}

?>