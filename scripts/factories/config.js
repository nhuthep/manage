(function() {
  app.factory('config', [
    'urls', '$http', '$rootScope', function(urls, $http, $rootScope) {
      var factory;
      return factory = {
        load: function() {
          return $http({
            url: 'data/config.json',
            method: "GET"
          }).success(function(data, status, headers, config) {});
        }
      };
    }
  ]);

}).call(this);
