(function() {
  app.factory('login', [
    'urls', '$http', '$rootScope', '$location', function(urls, $http, $rootScope, $location) {
      var factory;
      return factory = {
        login: function(username, password, company_code) {
          if (!username) {
            username = localStorage.getItem('username');
          } else {
            localStorage.setItem('username', username);
          }
          if (!password) {
            password = localStorage.getItem('password');
          } else {
            localStorage.setItem('password', password);
          }
          if (!company_code) {
            company_code = localStorage.getItem('company_code');
          } else {
            localStorage.setItem('company_code', company_code);
          }

          return $http({
            url: urls.login,
            method: "POST",
            data: {
              username: username,
              password: password,
              company_code: company_code
            },
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).success(function(data, status, headers, config) {
            if (!data[0].db) {
              return $location.path("general_login");
            } else {
              if (data[1].users) {
                if (data[1].users[0]) {
                  $rootScope.username = data[1].users[0].username;
                  $rootScope.usertype = data[1].users[0].type;
                  $rootScope.userrealname = data[1].users[0].realname;
                  $rootScope.userId = data[1].users[0].Id;
                  $rootScope.userpermission = data[1].users[0].permission;
                  return $location.path("general_home");
                } else {
                  return $location.path("general_login");
                }
              } else {
                return $location.path("general_login");
              }
            }
          });
        }
      };
    }
  ]);

}).call(this);
