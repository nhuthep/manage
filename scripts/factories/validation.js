(function() {
  app.factory('validation', [
    '$rootScope', function($rootScope) {
      var factory;
      return factory = {
        email: function(email) {
          var ret;
          ret = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return ret.test(email);
        },
        mobile: function(mobile) {
          if (!$.isNumeric(mobile)) {
            return false;
          } else if (mobile > 9999999999) {
            return false;
          } else {
            return true;
          }
        },
        postcode: function(postcode) {
          if (!$.isNumeric(postcode)) {
            return false;
          } else if (postcode > 9999) {
            return false;
          } else {
            return true;
          }
        }
      };
    }
  ]);

}).call(this);
