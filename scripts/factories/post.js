(function() {
  app.factory('post', [
    'urls', '$http', '$rootScope', '$cookies', '$cacheFactory', function(urls, $http, $rootScope, $cookies, $cacheFactory) {
      var factory;
      return factory = {
        load: function(postdata, prevent_duplicated, cache) {
          var key, url, _i, _len, _ref;
          if (prevent_duplicated) {
            $rootScope.post_status = "Loading " + postdata["from"];
            $rootScope.loading = true;
          }
          if (postdata.method === "get") {
            url = urls.post;
            if (!$rootScope.loadedCache) {
              $http({
                url: 'data/cachedVersion.json',
                method: "GET"
              }).success(function(data, status, headers, config) {
                var $httpDefaultCache;
                if (parseInt(data.version) !== $cookies.version) {
                  $cookies.version = parseInt(data.version);
                  $httpDefaultCache = $cacheFactory.get('$http').removeAll();
                }
                return $rootScope.loadedCache = true;
              });
            }
            _ref = postdata.describe;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              key = _ref[_i];
              if (url !== urls.post) {
                url += "&";
              } else {
                url += "?";
              }
              url += key + "=" + postdata[key];
            }
            return $http({
              cache: cache,
              url: url,
              method: "GET",
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
              }
            }).success(function(data, status, headers, config) {
              if (prevent_duplicated) {
                $rootScope.post_status = "Success!";
                return $rootScope.loading = false;
              }
            }).error(function(data, status, headers, config) {
              if (prevent_duplicated) {
                $rootScope.post_status = "Error";
                return setTimeout(function() {
                  return location.reload();
                }, 10000);
              }
            });
          } else {
            return $http({
              url: urls.post,
              method: "POST",
              data: postdata,
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
              }
            }).success(function(data, status, headers, config) {
              if (prevent_duplicated) {
                $rootScope.post_status = "Success!";
                return $rootScope.loading = false;
              }
            }).error(function(data, status, headers, config) {
              if (prevent_duplicated) {
                $rootScope.post_status = "Error!";
                return setTimeout(function() {
                  return location.reload();
                }, 10000);
              }
            });
          }
        }
      };
    }
  ]);

}).call(this);
