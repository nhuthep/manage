(function() {
  app.factory('date', function(urls, $http) {
    var factory, today, tomorrow;
    today = function(date) {
      var d, str, _today;
      str = date.split("-");
      d = new Date(str[2], str[1] - 1, str[0]);
      _today = new Date();
      return d.getDate() === _today.getDate() && d.getMonth() === _today.getMonth() && d.getFullYear() === _today.getFullYear();
    };
    tomorrow = function(date) {
      var d, str, _tomorrow;
      str = date.split("-");
      d = new Date(str[2], str[1] - 1, str[0]);
      _tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
      return d.getDate() === _tomorrow.getDate() && d.getMonth() === _tomorrow.getMonth() && d.getFullYear() === _tomorrow.getFullYear();
    };
    return factory = {
      ddmmyyyy: function(date) {
        var d, formattedDate, m, y;
        if (date === void 0) {
          formattedDate = new Date();
        } else {
          formattedDate = new Date(date);
        }
        d = formattedDate.getDate();
        if (d < 10) {
          d = '0' + d;
        }
        m = formattedDate.getMonth() + 1;
        if (m < 10) {
          m = '0' + m;
        }
        y = formattedDate.getFullYear();
        return d + "-" + m + "-" + y;
      },
      yyyymmdd: function(date) {
        var d, formattedDate, m, y;
        if (date === void 0) {
          formattedDate = new Date();
        } else {
          formattedDate = new Date(date);
        }
        d = formattedDate.getDate();
        if (d < 10) {
          d = '0' + d;
        }
        m = formattedDate.getMonth() + 1;
        if (m < 10) {
          m = '0' + m;
        }
        y = formattedDate.getFullYear();
        return y + "-" + m + "-" + d;
      },
      mmddyyyy: function(date) {
        var d, formattedDate, m, y;
        if (date === void 0) {
          formattedDate = new Date();
        } else {
          formattedDate = new Date(date);
        }
        d = formattedDate.getDate();
        if (d < 10) {
          d = '0' + d;
        }
        m = formattedDate.getMonth() + 1;
        if (m < 10) {
          m = '0' + m;
        }
        y = formattedDate.getFullYear();
        return m + "/" + d + "/" + y;
      },
      hhmmss: function(date) {
        var d, formattedDate, h, m, s;
        if (date === void 0) {
          formattedDate = new Date();
        } else {
          formattedDate = new Date(date);
        }
        h = formattedDate.getHours();
        if (h < 10) {
          d = '0' + d;
        }
        m = formattedDate.getMinutes();
        if (m < 10) {
          m = '0' + m;
        }
        s = formattedDate.getSeconds();
        if (s < 10) {
          s = '0' + s;
        }
        return h + ":" + m + ":" + s;
      },
      md_ddmmyyyy: function(date) {
        var d;
        d = date.split("-");
        return new Date(d[2], d[1] - 1, d[0]);
      },
      md_yyyymmdd: function(date) {
        var d;
        d = date.split("-");
        return new Date(d[0], d[1] - 1, d[2]);
      },
      dow: function(date) {
        var d, n, str, weekday;
        str = date.split("-");
        d = new Date(str[2], str[1] - 1, str[0]);
        weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        n = weekday[d.getDay()];
        if (today(date)) {
          n = "Today";
        }
        if (tomorrow(date)) {
          n = "Tomorrow";
        }
        return n;
      },
      nextdate: function(date) {
        var d, str, _nextdate;
        str = date.split("-");
        d = new Date(str[2], str[1], str[0]);
        _nextdate = new Date(d.getTime() + 24 * 60 * 60 * 1000);
        return str = _nextdate.getFullYear() + "-" + _nextdate.getMonth() + "-" + _nextdate.getDate();
      },
      istoday: function(date) {
        if (today(date)) {
          return true;
        } else {
          return false;
        }
      },
      thisdate: function() {
        var d, date, month;
        d = new Date;
        date = d.getDate();
        if (date < 10) {
          date = "0" + date;
        }
        month = d.getMonth() + 1;
        if (month < 10) {
          month = "0" + month;
        }
        return d.getFullYear() + "-" + month + "-" + date;
      },
      nextdate: function(days) {
        var d, date, month, td;
        td = new Date;
        d = new Date(td.getTime() + 24 * 60 * 60 * 1000 * days);
        date = d.getDate();
        if (date < 10) {
          date = "0" + date;
        }
        month = d.getMonth() + 1;
        if (month < 10) {
          month = "0" + month;
        }
        return d.getFullYear() + "-" + month + "-" + date;
      },
      getDaysArray: function(month, year) {
        var daysArray, daysInWeek, daysIndex, dd, i, index, l, mm, numDaysInMonth;
        numDaysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        daysInWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        daysIndex = {
          'Sun': 0,
          'Mon': 1,
          'Tue': 2,
          'Wed': 3,
          'Thu': 4,
          'Fri': 5,
          'Sat': 6
        };
        index = daysIndex[(new Date(year, month - 1, 1)).toString().split(' ')[0]];
        daysArray = [];
        i = 0;
        l = numDaysInMonth[month - 1];
        if (month < 10) {
          mm = '0' + month;
        } else {
          mm = month;
        }
        while (i < l) {
          if (i < 9) {
            dd = '0' + (i + 1);
          } else {
            dd = i + 1;
          }
          daysArray.push({
            date: i + 1,
            fulldate: year + '-' + mm + '-' + dd,
            dow: index,
            dname: daysInWeek[index++]
          });
          if (index === 7) {
            index = 0;
          }
          i += 1;
        }
        return daysArray;
      }
    };
  });

}).call(this);
