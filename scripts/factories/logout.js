(function() {
    app.factory('logout', [
        'urls', '$http', '$rootScope', '$location', function(urls, $http, $rootScope, $location) {
            var factory;
            return factory = {
                logout: function () {
                    $http.get(urls.logout).then(function() {
                        localStorage.removeItem('username');
                        localStorage.removeItem('password');
                        localStorage.removeItem('password');
                        //localStorage.clear();
                        return $location.path("general_login");
                    });
                }
            }
        }
    ]);

}).call(this);
