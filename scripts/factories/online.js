(function() {
  app.factory('online', [
    '$window', '$rootScope', function($window, $rootScope) {
      var onlineStatus;
      onlineStatus = {
        onLine: $window.navigator.onLine,
        isOnline: function() {
          return onlineStatus.onLine;
        }
      };
      $window.addEventListener("online", function() {
        onlineStatus.onLine = true;
        return $rootScope.$digest();
      }, true);
      $window.addEventListener("offline", function() {
        onlineStatus.onLine = false;
        return $rootScope.$digest();
      }, true);
      return onlineStatus;
    }
  ]);

}).call(this);
