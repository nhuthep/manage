(function() {
  app.config([
    '$interpolateProvider', function($interpolateProvider) {
      $interpolateProvider.startSymbol('{{');
      $interpolateProvider.endSymbol('}}');
      return null;
    }
  ]);

}).call(this);
