/**
 * Created by nhuthep on 10/8/15.
 */

(function() {
    app.controller('admin_agency', [
        '$http', '$scope', '$rootScope', '$location', 'post', function($http, $scope, $rootScope, $location, post) {
            var getData;
            if (!$rootScope.username) {
                return $location.path("general_login");
            } else if (!$rootScope.data_loaded) {
                return $location.path("general_home");
            } else {
                $scope.sort = 'type';
                $scope.revert = false;
                $scope.filterType = "";
                $scope.cancelData = function(reload) {
                    $scope.edit_id = 0;
                    $scope.action = $rootScope.lang.add;
                    $scope.cta = $rootScope.lang.add_agency;
                    $scope.manager = "";
                    $scope.location = "";
                    $scope.type = 1;
                    $scope.code = "";
                    $scope.commission = 0;
                    $scope.actived = "1";
                    if (reload) {
                        return getData();
                    }
                };
                getData = function() {
                    var data;
                    data = {
                        method: "get",
                        from: "partners",
                        describe: ["from", "agency"],
                        agency: 1
                    };
                    return post.load(data).then(function(returnData) {
                        data = returnData.data;
                        $rootScope.agency_list = [];
                        $rootScope.agency_byid = [];
                        for (var i = 0; i < data.length; i++) {
                            var item = {
                                id: data[i][0],
                                location: data[i][1],
                                manager: data[i][2],
                                actived: data[i][5],
                                commission: data[i][6],
                                code: data[i][7],
                                type: data[i][3]
                            };
                            if (item.manager) {
                                $rootScope.agency_list.push(item);
                            }
                        };

                        var agency_list = $rootScope.agency_list;
                        if (agency_list.length) {
                            for (var i = 0; i < agency_list.length; i++) {
                                $rootScope.agency_list[i].location_name = $rootScope.city_byid[agency_list[i].location].name;
                                $rootScope.agency_byid[agency_list[i].Id] = agency_list[i];
                            }
                        }

                        return $scope.filterData();
                    });
                };
                $scope.postData = function() {
                    var postData;
                    postData = {
                        method: "post",
                        from: "partners",
                        describe: ["code", "manager", "id", "location", "type", "actived", "commission", "cost_type"],
                        code: $scope.code,
                        manager: $scope.manager,
                        location: $scope.location,
                        type: 1,
                        commission: $scope.commission,
                        id: $scope.edit_id,
                        actived: $scope.actived,
                        cost_type: 1
                    };
                    if ($scope["delete"] === true) {
                        postData["delete"] = true;
                        postData.describe.push("delete");
                    }
                    return post.load(postData).then(function(data) {
                        $rootScope.agency_byid[$scope.edit_id] = postData;
                        return $scope.cancelData(true);
                    });
                };
                $scope.editData = function(id) {
                    $scope.action = $rootScope.lang.edit;
                    $scope.cta = $rootScope.lang.edit_agency;
                    $scope.edit_id = $rootScope.agency_byid[id].Id;
                    $scope.code = $rootScope.agency_byid[id].code;
                    $scope.manager = $rootScope.agency_byid[id].manager;
                    $scope.location = $rootScope.agency_byid[id].location;
                    $scope.type = 1;
                    $scope.commission = parseInt($rootScope.agency_byid[id].commission);
                    $scope.actived = $rootScope.agency_byid[id].actived;
                    return document.getElementById("first").focus();
                };
                $scope.deleteData = function() {
                    $scope["delete"] = true;
                    return $scope.postData;
                };
                $scope.filterData = function() {
                    var item, _i, _len, _ref, _results;
                    _ref = $rootScope.agency_list;
                    _results = [];
                    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                        item = _ref[_i];
                        if (item.type === $scope.filterType || $scope.filterType === "") {
                            _results.push(item.showed = 1);
                        } else {
                            _results.push(item.showed = 0);
                        }
                    }
                    return _results;
                };
                $scope.sortBy = function(field) {
                    if ($scope.sort === field) {
                        return $scope.revert = !$scope.revert;
                    } else {
                        $scope.sort = field;
                        return $scope.revert = false;
                    }
                };
                return $scope.cancelData(true);
            }
        }
    ]);

}).call(this);

