(function() {
  app.controller('finance_year', [
    '$scope', '$rootScope', '$location', 'post', function($scope, $rootScope, $location, post) {
      var dt, getInput, getShare, input, _i, _ref, _ref1, _results;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.inputs = [];
        input = {
          type: [],
          partner: [],
          city: []
        };
        $scope.total = {
          type: [],
          partner: [],
          city: []
        };
        $scope.view_type = 0;
        $scope.getOrder = function() {
          var data, dt, ndt, next_year, where;
          dt = $scope.year + "-1-1";
          next_year = $scope.year + 1;
          ndt = next_year + "-1-1";
          where = '`start_date` >= "' + dt + '" AND `start_date` < "' + ndt + '"';
          data = {
            method: "get",
            from: "orders",
            where: where,
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            var item, _i, _len, _ref;
            $rootScope.order_list = data.data;
            $rootScope.order_byid = [];
            _ref = $rootScope.order_list;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              $scope.total_income += parseInt(item.total);
              $rootScope.order_byid[item.Id] = item;
            }
            return getInput();
          });
        };
        getShare = function() {
          var data, where;
          where = '`year` = "' + $scope.year + '"';
          data = {
            method: "get",
            from: "shared_outcome",
            where: where,
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            var item, _i, _len, _ref, _results;
            $scope.shared_list = data.data;
            $scope.shared_byid = [];
            $scope.total_share = 0;
            _ref = $scope.shared_list;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              if ($rootScope.share_type_byid) {
                if ($rootScope.share_type_byid[item.share_type]) {
                  item.share_type_name = $rootScope.share_type_byid[item.share_type].name;
                }
              }
              if ($rootScope.partner_byid) {
                if ($rootScope.partner_byid[item.agent]) {
                  item.agent_name = $rootScope.partner_byid[item.agent].name;
                }
              }
              $scope.shared_byid[item.Id] = item;
              _results.push($scope.total_share += parseInt(item.amount));
            }
            return _results;
          });
        };
        getInput = function() {
          var data, dt, ndt, next_year, where;
          getShare();
          $scope.total_benefit = 0;
          $scope.total_in = 0;
          $scope.total_out = 0;
          $scope.total_debt = 0;
          dt = $scope.year + "-1-1";
          next_year = $scope.year + 1;
          ndt = next_year + "-1-1";
          where = '`date` >= "' + dt + '" AND `date` < "' + ndt + '"';
          data = {
            method: "get",
            from: "input",
            where: where,
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            var amount, item, locationId, partnerId, partnerTypeId, _i, _j, _k, _l, _len, _len1, _len2, _len3, _ref, _ref1, _ref2, _ref3, _results;
            $scope.inputs = data.data;
            $scope.input_byid = [];
            if ($rootScope.partner_type_byid && $rootScope.partner_byid && $rootScope.city_byid) {
              _ref = data.data;
              for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                item = _ref[_i];
                item.active = 1;
                partnerId = item.partner;
                item.partner_name = $rootScope.partner_byid[partnerId].name;
                partnerTypeId = $rootScope.partner_byid[partnerId].type;
                locationId = $rootScope.partner_byid[partnerId].location;
                if (input.type[partnerTypeId] === void 0) {
                  input.type[partnerTypeId] = {
                    total: 0,
                    name: $rootScope.partner_type_byid[partnerTypeId].name,
                    percent: 0,
                    inout: "-"
                  };
                }
                if (input.partner[partnerId] === void 0) {
                  input.partner[partnerId] = {
                    total: 0,
                    name: $rootScope.partner_byid[partnerId].manager,
                    percent: 0,
                    inout: "-"
                  };
                }
                if (input.city[locationId] === void 0) {
                  input.city[locationId] = {
                    total: 0,
                    name: $rootScope.city_byid[locationId].name,
                    percent: 0,
                    inout: "-"
                  };
                }
                amount = parseInt(item.amount);
                if (item.inout === "0") {
                  $scope.total_in += amount;
                  input.type[partnerTypeId].total -= amount;
                  input.partner[partnerId].total -= amount;
                  input.city[locationId].total -= amount;
                }
                if (item.inout === "1") {
                  $scope.total_out += amount;
                  input.type[partnerTypeId].total += amount;
                  input.partner[partnerId].total += amount;
                  input.city[locationId].total += amount;
                }
                if (item.inout === "2") {
                  $scope.total_debt += amount;
                }
                $scope.input_byid[item.Id] = item;
              }
              $scope.total_outcome = $scope.total_debt + $scope.total_share;
              $scope.total_debt_increase = $scope.total_debt - $scope.total_out + $scope.total_in - $scope.total_income;
              $scope.total_benefit = $scope.total_income - $scope.total_debt - $scope.total_share;
              _ref1 = input.type;
              for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
                item = _ref1[_j];
                if (item !== void 0) {
                  if (item.total > 0) {
                    item.inout = $scope.inout_list[1];
                    item.percent = Math.floor(item.total / $scope.total_out * 1000) / 10;
                  }
                  if (item.total < 0) {
                    item.inout = $scope.inout_list[0];
                    item.percent = Math.floor(item.total / $scope.total_in * 1000) / 10;
                  }
                  $scope.total.type.push(item);
                }
              }
              _ref2 = input.partner;
              for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
                item = _ref2[_k];
                if (item !== void 0) {
                  if (item.total > 0) {
                    item.inout = $scope.inout_list[1];
                    item.percent = Math.floor(item.total / $scope.total_out * 1000) / 10;
                  }
                  if (item.total < 0) {
                    item.inout = $scope.inout_list[0];
                    item.percent = Math.floor(item.total / $scope.total_in * 1000) / 10;
                  }
                  $scope.total.partner.push(item);
                }
              }
              _ref3 = input.city;
              _results = [];
              for (_l = 0, _len3 = _ref3.length; _l < _len3; _l++) {
                item = _ref3[_l];
                if (item !== void 0) {
                  if (item.total > 0) {
                    item.inout = $scope.inout_list[1];
                    item.percent = Math.floor(item.total / $scope.total_out * 1000) / 10;
                  }
                  if (item.total < 0) {
                    item.inout = $scope.inout_list[0];
                    item.percent = Math.floor(item.total / $scope.total_in * 1000) / 10;
                  }
                  _results.push($scope.total.city.push(item));
                } else {
                  _results.push(void 0);
                }
              }
              return _results;
            }
          });
        };
        $scope.editData = function(id, share) {
          if (share) {
            return $location.path("action_share/" + id);
          } else {
            return $location.path("action_inout/" + id);
          }
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
        dt = new Date();
        $scope.year = dt.getFullYear();
        $scope.year_list = (function() {
          _results = [];
          for (var _i = _ref = $scope.start_year, _ref1 = $scope.year; _ref <= _ref1 ? _i <= _ref1 : _i >= _ref1; _ref <= _ref1 ? _i++ : _i--){ _results.push(_i); }
          return _results;
        }).apply(this);
        return $scope.getOrder();
      }
    }
  ]);

}).call(this);
