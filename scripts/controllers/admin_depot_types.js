(function() {
  app.controller('admin_depot_types', [
    '$scope', '$rootScope', '$location', 'post', function($scope, $rootScope, $location, post) {
      var getData;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.sort = 'cost_types';
        $scope.revert = true;
        $scope.cancelData = function(reload) {
          $scope.edit_id = 0;
          $scope.action = $rootScope.lang.add;
          $scope.cta = $rootScope.lang.add_depot_type;
          $scope.name = "";
          $scope.actived = "1";
          if (reload) {
            return getData();
          }
        };
        getData = function() {
          var data;
          data = {
            method: "get",
            from: "depot_types",
            describe: ["from"]
          };
          return post.load(data).then(function(data) {
            var item, _i, _len, _ref, _results;
            $scope.depot_types = data.data;
            $scope.depot_type_byid = [];
            if ($rootScope.cost_type_byid) {
              if ($rootScope.cost_type_byid) {
                _ref = $scope.depot_types;
                _results = [];
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                  item = _ref[_i];
                  item.cost_type_name = $rootScope.cost_type_byid[item.cost_type].name;
                  if ($rootScope.unit_byid[item.unit]) {
                    item.unit_name = $rootScope.unit_byid[item.unit].name;
                  }
                  _results.push($rootScope.depot_type_byid[item.Id] = item);
                }
                return _results;
              }
            }
          });
        };
        $scope.postData = function() {
          var postData;
          postData = {
            method: "post",
            from: "depot_types",
            describe: ["name", "id", "cost_type", "actived", "unit"],
            name: $scope.name,
            cost_type: $scope.cost_type,
            id: $scope.edit_id,
            actived: $scope.actived,
            unit: $scope.unit
          };
          if ($scope["delete"] === true) {
            postData["delete"] = true;
            postData.describe.push("delete");
          }
          return post.load(postData).then(function(data) {
            $rootScope.depot_type_byid[$scope.edit_id] = postData;
            return $scope.cancelData(true);
          });
        };
        $scope.editData = function(id) {
          $scope.action = $rootScope.lang.edit;
          $scope.cta = $rootScope.lang.edit_depot_type;
          $scope.edit_id = $rootScope.depot_type_byid[id].Id;
          $scope.name = $rootScope.depot_type_byid[id].name;
          $scope.cost_type = $rootScope.depot_type_byid[id].cost_type;
          $scope.unit = $rootScope.depot_type_byid[id].unit;
          $scope.actived = $rootScope.depot_type_byid[id].actived;
          return document.getElementById("first").focus();
        };
        $scope.deleteData = function() {
          $scope["delete"] = true;
          return $scope.postData;
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
        return $scope.cancelData(true);
      }
    }
  ]);

}).call(this);
