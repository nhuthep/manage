(function() {
  app.controller('employee_profile', [
    '$scope', '$rootScope', '$routeParams', '$cookies', 'post', 'date', function($scope, $rootScope, $routeParams, $cookies, post, date) {
      var getData;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.edit_id = 0;
        $scope.user = "";
        $scope.cancelData = function(stop) {
          $scope.name = "";
          $scope.phone = "";
          $scope.address = "";
          $scope.email = "";
          $scope.position = "";
          $scope.dob = "";
          $scope.start_date = "";
          $scope.gender = "";
          $scope.salary = 0;
          $scope.comment = "";
          if (!stop) {
            $scope.edit_id = $routeParams.id;
            return getData();
          } else {
            return $scope.edit_id = 0;
          }
        };
        getData = function() {
          var data;
          if ($scope.edit_id > 0) {
            data = {
              method: "get",
              from: "staffs",
              where: "`Id` = " + $scope.edit_id,
              describe: ["from", "where"]
            };
            return post.load(data, true).then(function(data) {
              var edit_data;
              edit_data = data.data[0];
              $scope.name = edit_data.name;
              $scope.phone = edit_data.phone;
              $scope.address = edit_data.address;
              $scope.email = edit_data.email;
              $scope.position = edit_data.position;
              $scope.dob = edit_data.dob;
              $scope.start_date = edit_data.start_date;
              $scope.gender = edit_data.gender;
              $scope.salary = parseInt(edit_data.salary);
              return $scope.comment = edit_data.comment;
            });
          }
        };
        $scope.postData = function() {
          var postData;
          postData = {
            method: "post",
            from: "staffs",
            describe: ["id", "name", "phone", "address", "email", "position", "dob", "start_date", "gender", "salary", "comment", "actived"],
            id: $scope.edit_id,
            name: $scope.name,
            phone: $scope.phone,
            address: $scope.address,
            email: $scope.email,
            position: $scope.position,
            dob: $scope.dob,
            start_date: $scope.start_date,
            gender: $scope.gender,
            salary: $scope.salary,
            comment: $scope.comment,
            actived: true
          };
          if ($scope["delete"] === true) {
            postData.actived = false;
          }
          return post.load(postData, true).then(function(data) {
            $rootScope.staff_byid[$scope.edit_id] = postData;
            return $scope.cancelData(true);
          });
        };
        $scope.deleteData = function() {
          $scope["delete"] = true;
          return $scope.postData();
        };
        $scope.open = function($event) {
          $event.preventDefault();
          $event.stopPropagation();
          return $scope.opened = true;
        };
        $scope.dateOptions = {
          'year-format': "'yy'",
          'starting-day': 1
        };
        return $scope.cancelData();
      }
    }
  ]);

}).call(this);
