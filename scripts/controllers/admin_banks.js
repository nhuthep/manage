(function() {
  app.controller('admin_banks', [
    '$scope', '$rootScope', '$location', 'post', function($scope, $rootScope, $location, post) {
      var getData;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.sort = 'actived';
        $scope.revert = true;
        $scope.cancelData = function(reload) {
          $scope.name = "";
          $scope.actived = "1";
          $scope.edit_id = 0;
          $scope.action = $rootScope.lang.add;
          $scope.cta = $rootScope.lang.add_bank;
          if (reload) {
            return getData();
          }
        };
        getData = function() {
          var postData;
          postData = {
            method: "get",
            from: "banks",
            describe: ["from"]
          };
          return post.load(postData, true).then(function(data) {
            var item, _i, _len, _ref, _results;
            $rootScope.bank_list = data.data;
            $rootScope.bank_byid = [];
            _ref = $rootScope.bank_list;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              _results.push($rootScope.bank_byid[item.Id] = item);
            }
            return _results;
          });
        };
        $scope.postData = function() {
          var postData;
          postData = {
            method: "post",
            from: "banks",
            describe: ["name", "id", "actived"],
            name: $scope.name,
            id: $scope.edit_id,
            actived: $scope.actived
          };
          if ($scope["delete"] === true) {
            postData["delete"] = true;
            postData.describe.push("delete");
          }
          return post.load(postData).then(function(data) {
            $rootScope.bank_byid[$scope.edit_id] = postData;
            return $scope.cancelData(true);
          });
        };
        $scope.editData = function(id) {
          $scope.action = $rootScope.lang.edit;
          $scope.cta = $rootScope.lang.edit_bank;
          $scope.edit_id = $rootScope.bank_byid[id].Id;
          $scope.name = $rootScope.bank_byid[id].name;
          $scope.actived = $rootScope.bank_byid[id].actived;
          return document.getElementById("first").focus();
        };
        $scope.deleteData = function() {
          $scope["delete"] = true;
          return $scope.postData;
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
        return $scope.cancelData(true);
      }
    }
  ]);

}).call(this);
