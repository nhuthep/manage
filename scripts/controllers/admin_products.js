(function() {
    app.controller('admin_products', [
        '$scope', '$rootScope', '$location', 'post', function($scope, $rootScope, $location, post) {
            if (!$rootScope.username) {
                return $location.path("general_login");
            } else if (!$rootScope.data_loaded) {
                return $location.path("general_home");
            } else {
                $scope.sort = "name";
                $scope.revert = false;
                $scope.cancelData = function(reload) {
                    $scope.name = "";
                    $scope.price = "";
                    $scope.unit_type = "";
                    $scope.description = "";
                    $scope.edit_id = 0;
                    $scope.action = $rootScope.lang.add;
                    $scope.cta = $rootScope.lang.add_product;
                    $scope.is_active = "1";
                    if (reload) {
                        return $scope.getData();
                    }
                };
                $scope.getData = function() {
                    var data;
                    data = {
                        method: "get",
                        from: "products",
                        describe: ["from"]
                    };
                    return post.load(data, true).then(function(data) {
                        var item, _i, _len, _ref, _results;
                        $rootScope.product_list = data.data;
                        $rootScope.product_byid = [];
                        _ref = $rootScope.product_list;
                        _results = [];
                        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                            item = _ref[_i];
                            //item.type_name = $rootScope.product_types[item.unit_type];
                            _results.push($rootScope.product_byid[item.Id] = item);
                        }
                        if ($rootScope.product_byid.length == 0) {
                            $rootScope.showAddProduct = 1;
                        } else {
                            $rootScope.showAddProduct = 0;
                        }
                        return _results;
                    });
                };
                $scope.postData = function() {
                    var postData;
                    postData = {
                        method: "post",
                        from: "products",
                        describe: ["Id", "name", "price", "unit_type", "description", "is_active"],
                        id: $scope.edit_id,
                        name: $scope.name,
                        price: $scope.price,
                        unit_type: $scope.unit_type,
                        description: $scope.description,
                        is_active: $scope.is_active
                    };
                    $scope.cancelData();
                    if ($scope["delete"] === true) {
                        postData["delete"] = true;
                        postData.describe.push("delete");
                    }
                    return post.load(postData).then(function(data) {
                        $rootScope.product_byid[$scope.edit_id] = postData;
                        return $scope.cancelData(true);
                    });
                };
                $scope.editData = function(id) {
                    $scope.action = $rootScope.lang.edit;
                    $scope.cta = $rootScope.lang.edit_product;
                    $scope.edit_id = $rootScope.product_byid[id].Id;
                    $scope.name = $rootScope.product_byid[id].name;
                    $scope.price = $rootScope.product_byid[id].price;
                    $scope.unit_type = $rootScope.product_byid[id].unit_type;
                    $scope.description = $rootScope.product_byid[id].description;
                    $scope.is_active = $rootScope.product_byid[id].is_active;
                    $("html, body").animate({
                        scrollTop: 0
                    }, 500);
                    return document.getElementById("first").focus();
                };
                $scope.deleteData = function() {
                    $scope["delete"] = true;
                    return $scope.postData;
                };
                $scope.sortBy = function(field) {
                    if ($scope.sort === field) {
                        return $scope.revert = !$scope.revert;
                    } else {
                        $scope.sort = field;
                        return $scope.revert = false;
                    }
                };

                $scope.cancelData();
                return $scope.getData();
            }
        }
    ]);

}).call(this);
