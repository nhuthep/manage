(function() {
  app.controller('task_date_list', [
    '$scope', '$rootScope', '$location', 'post', 'date', function($scope, $rootScope, $location, post, date) {
      var dt, getDate, getOrder, getSchedule, _i, _ref, _ref1, _results;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        getOrder = function() {
          var data, month, nextmonth, nextyear, where;
          if ($scope.month === 12) {
            nextmonth = 1;
            nextyear = $scope.year + 1;
          } else {
            nextmonth = $scope.month + 1;
            nextyear = $scope.year;
          }
          if ($scope.month < 10) {
            month = '0' + $scope.month;
          } else {
            month = $scope.month;
          }
          if (nextmonth < 10) {
            nextmonth = '0' + nextmonth;
          }
          where = '`end_date` >= "' + $scope.year + '-' + month + '-01" AND `start_date` < "' + nextyear + '-' + nextmonth + '-01"';
          data = {
            method: "get",
            from: "orders",
            describe: ["from"]
          };
          return post.load(data, true, true).then(function(data) {
            var item, _i, _len, _ref;
            $scope.order_list = data.data;
            $scope.order_byid = [];
            _ref = $scope.order_list;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              $scope.order_byid[item.Id] = item;
            }
            return $scope.getTaskDaily();
          });
        };
        getDate = function() {
          var item, _i, _len, _ref, _results;
          $scope.date_list = date.getDaysArray($scope.month, $scope.year);
          $scope.date_byid = {};
          _ref = $scope.date_list;
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            item = _ref[_i];
            item.Id = 0;
            item.task_list = [];
            item.task_schedule = [];
            item.total_task_hours = 0;
            item.total_schedule_hours = 0;
            $scope.date_byid[item.fulldate] = item;
            if (item.dow === 0) {
              _results.push(item.type = 2);
            } else {
              _results.push(item.type = 0);
            }
          }
          return _results;
        };
        $scope.getTaskDaily = function() {
          var data, month, nextmonth, nextyear, where;
          getDate();
          if ($scope.month === 12) {
            nextmonth = 1;
            nextyear = $scope.year + 1;
          } else {
            nextmonth = $scope.month + 1;
            nextyear = $scope.year;
          }
          if ($scope.month < 10) {
            month = '0' + $scope.month;
          } else {
            month = $scope.month;
          }
          if (nextmonth < 10) {
            nextmonth = '0' + nextmonth;
          }
          where = '`date` >= "' + $scope.year + '-' + month + '-01" AND `date` < "' + nextyear + '-' + nextmonth + '-01"';
          data = {
            method: "get",
            from: "daily_tasks",
            where: where,
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            var item, _i, _len, _ref;
            $scope.daily_tasks = data.data;
            _ref = $scope.daily_tasks;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              $scope.date_byid[item.date].task_list.push(item);
              $scope.date_byid[item.date].total_task_hours += parseInt(item.hours);
            }
            return getSchedule(where);
          });
        };
        getSchedule = function(where) {
          var data;
          data = {
            method: "get",
            from: "task_schedule",
            where: where,
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            var item, task, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _ref2, _results;
            $scope.task_schedule = data.data;
            _ref = $scope.task_schedule;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              $scope.date_byid[item.date].task_schedule.push(item);
              $scope.date_byid[item.date].total_schedule_hours += parseInt(item.hours);
            }
            _ref1 = $scope.date_list;
            _results = [];
            for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
              item = _ref1[_j];
              _ref2 = item.task_list;
              for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
                task = _ref2[_k];
                task.col = task.hours / item.total_task_hours * 100;
              }
              _results.push((function() {
                var _l, _len3, _ref3, _results1;
                _ref3 = item.task_schedule;
                _results1 = [];
                for (_l = 0, _len3 = _ref3.length; _l < _len3; _l++) {
                  task = _ref3[_l];
                  _results1.push(task.scol = task.hours / item.total_schedule_hours * 100);
                }
                return _results1;
              })());
            }
            return _results;
          });
        };
        $scope.month_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        dt = new Date();
        $scope.month = dt.getMonth();
        $scope.month += 1;
        $scope.year = dt.getFullYear();
        $scope.year_list = (function() {
          _results = [];
          for (var _i = _ref = $scope.start_year, _ref1 = $scope.year; _ref <= _ref1 ? _i <= _ref1 : _i >= _ref1; _ref <= _ref1 ? _i++ : _i--){ _results.push(_i); }
          return _results;
        }).apply(this);
        return getOrder();
      }
    }
  ]);

}).call(this);
