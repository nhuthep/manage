(function() {
  app.controller('admin_partners', [
    '$scope', '$rootScope', '$location', 'post', function($scope, $rootScope, $location, post) {
      var getData;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.sort = 'type';
        $scope.revert = false;
        $scope.filterType = "";
        $scope.cancelData = function(reload) {
          $scope.edit_id = 0;
          $scope.action = $rootScope.lang.add;
          $scope.cta = $rootScope.lang.add_partner;
          $scope.manager = "";
          $scope.location = "";
          $scope.type = "";
          $scope.code = "";
          $scope.cost_type = "";
          $scope.commission = 0;
          $scope.actived = "1";
          if (reload) {
            return getData();
          }
        };
        getData = function() {
          var data;
          data = {
            method: "get",
            from: "partners",
            describe: ["from"]
          };
          return post.load(data).then(function(data) {
            var item, _i, _len, _ref;
            $rootScope.partner_list = data.data;
            $rootScope.partner_byid = [];
            if ($rootScope.partner_type_byid && $rootScope.city_byid) {
              _ref = $rootScope.partner_list;
              for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                item = _ref[_i];
                item.commission = parseInt(item.commission);
                if (item.cost_type > 0) {
                  item.cost_type_name = $rootScope.cost_type_byid[item.cost_type].name;
                } else {
                  item.cost_type_name = "-";
                }
                item.partner_type_name = $rootScope.partner_type_byid[item.type].name;
                item.location_name = $rootScope.city_byid[item.location].name;
                $rootScope.partner_byid[item.Id] = item;
              }
            }
            return $scope.filterData();
          });
        };
        $scope.postData = function() {
          var postData;
          postData = {
            method: "post",
            from: "partners",
            describe: ["code", "manager", "id", "location", "type", "actived", "commission", "cost_type"],
            code: $scope.code,
            manager: $scope.manager,
            location: $scope.location,
            type: $scope.type,
            commission: $scope.commission,
            id: $scope.edit_id,
            actived: $scope.actived,
            cost_type: $scope.cost_type
          };
          if ($scope["delete"] === true) {
            postData["delete"] = true;
            postData.describe.push("delete");
          }
          return post.load(postData).then(function(data) {
            $rootScope.partner_byid[$scope.edit_id] = postData;
            return $scope.cancelData(true);
          });
        };
        $scope.editData = function(id) {
          $scope.action = $rootScope.lang.edit;
          $scope.cta = $rootScope.lang.edit_partner;
          $scope.edit_id = $rootScope.partner_byid[id].Id;
          $scope.code = $rootScope.partner_byid[id].code;
          $scope.manager = $rootScope.partner_byid[id].manager;
          $scope.location = $rootScope.partner_byid[id].location;
          $scope.type = $rootScope.partner_byid[id].type;
          $scope.commission = parseInt($rootScope.partner_byid[id].commission);
          $scope.cost_type = $rootScope.partner_byid[id].cost_type;
          $scope.actived = $rootScope.partner_byid[id].actived;
          return document.getElementById("first").focus();
        };
        $scope.deleteData = function() {
          $scope["delete"] = true;
          return $scope.postData;
        };
        $scope.filterData = function() {
          var item, _i, _len, _ref, _results;
          _ref = $rootScope.partner_list;
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            item = _ref[_i];
            if (item.type === $scope.filterType || $scope.filterType === "") {
              _results.push(item.showed = 1);
            } else {
              _results.push(item.showed = 0);
            }
          }
          return _results;
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
        return $scope.cancelData(true);
      }
    }
  ]);

}).call(this);
