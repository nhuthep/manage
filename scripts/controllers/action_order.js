(function() {
  app.controller('action_order', [
    '$scope', '$rootScope', '$routeParams', 'post', 'date', '$location', function($scope, $rootScope, $routeParams, post, date, $location) {
      var getData, postOrder;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.pushedData = [];
        $scope.paper = 0;
        $scope.rate = [1, 1.6, 2.4];
        $scope.edit_id = 0;
        $scope.cancelData = function(stop) {
          $scope.code = "";
          $scope.agent = "";
          $scope.customer = "";
          $scope.price = 0;
          $scope.product = '';
          $scope.quantity = 300;
          $scope.start_date = date.thisdate();
          $scope.end_date = date.nextdate(7);
          $scope.additional = 0;
          $scope.deposit = 0;
          $scope.comment = "";
          $scope.debtid = 0;
          $scope.action = "Thêm";
          $scope.cta = "Thêm đơn hàng";
          $scope.total = 0;
          $scope.remain = 0;
          $scope.user = $rootScope.userrealname;
          if (!stop) {
            return $scope.edit_id = $routeParams.id;
          } else {
            return $scope.edit_id = 0;
          }
        };
        getData = function() {
          var data;
          if (parseInt($scope.edit_id) !== 0) {
            data = {
              method: "get",
              from: "orders",
              where: "`Id` = " + $scope.edit_id,
              describe: ["from", "where"]
            };
            return post.load(data, true).then(function(data) {
              var edit_data;
              edit_data = data.data[0];
              $scope.code = edit_data.code;
              $scope.agent = edit_data.agent;
              $scope.customer = edit_data.customer;
              $scope.price = parseInt(edit_data.price);
              $scope.quantity = parseInt(edit_data.quantity);
              $scope.start_date = edit_data.start_date;
              $scope.end_date = edit_data.end_date;
              $scope.additional = parseInt(edit_data.additional);
              $scope.deposit = parseInt(edit_data.deposit);
              $scope.total = parseInt(edit_data.total);
              $scope.comment = edit_data.comment;
              $scope.debtid = edit_data.debtid;
              $scope.paper = parseInt(edit_data.paper);
              $scope.user = edit_data.user;
              console.log($scope.price);
              $scope.changePrice();
              console.log($scope.price);
              $scope.action = "Sửa / Xóa";
              return $scope.cta = "Sửa / Xóa đơn hàng";
            });
          } else {
            if ($rootScope.partner > 0) {
              $scope.agent = $rootScope.partner;
              return $rootScope.partner = null;
            }
          }
        };
        $scope.postData = function() {
          var postData;
          if ($scope.agent_byid[$scope.agent].commission > 0) {
            postData = {
              method: "post",
              from: "input",
              describe: ["id", "amount", "inout", "partner", "date", "payment_type", "bank", "payment_code", "comment", "user"],
              id: $scope.debtid,
              amount: $scope.total * $scope.agent_byid[$scope.agent].commission / 100,
              inout: 2,
              partner: $scope.agent,
              date: $scope.start_date,
              payment_type: 0,
              bank: 1,
              payment_code: $scope.code,
              comment: "% hoa hồng đơn hàng " + $scope.code + ": " + $scope.agent_byid[$scope.agent].commission + "%",
              user: $rootScope.userrealname
            };
            if ($scope["delete"] === true) {
              postData["delete"] = true;
              postData.describe.push("delete");
            }
            return post.load(postData, true).then(function(data) {
              $scope.debtid = data.data;
              return postOrder();
            });
          } else {
            return postOrder();
          }
        };
        postOrder = function() {
          var postData;
          postData = {
            method: "post",
            from: "orders",
            describe: ["id", "code", "agent", "customer", "price", "product_id", "quantity", "start_date", "end_date", "additional", "deposit", "total", "comment", "debtid", "user"],
            id: $scope.edit_id,
            code: $scope.code,
            agent: $scope.agent,
            customer: $scope.customer,
            price: $scope.price,
            product_id: $scope.product,
            quantity: $scope.quantity,
            start_date: $scope.start_date,
            end_date: $scope.end_date,
            additional: $scope.additional,
            deposit: $scope.deposit,
            total: $scope.total,
            comment: $scope.comment,
            debtid: $scope.debtid,
            user: $scope.user
          };
          if ($scope["delete"] === true) {
            postData["delete"] = true;
            postData.describe.push("delete");
          }
          return post.load(postData, true).then(function(data) {
            $scope.pushedData.push(postData);
            return $scope.cancelData(true);
          });
        };
        $scope.changePrice = function() {
            $scope.price = parseInt($scope.product_byid[$scope.product].price);
            $scope.unit_type = parseInt($scope.product_byid[$scope.product].unit_type);
            return $scope.countTotal();
        };
        $scope.countTotal = function() {
          $scope.total = $scope.price * $scope.quantity + $scope.additional;
          return $scope.remain = $scope.total - $scope.deposit;
        };
        $scope.add1000 = function(id) {
          $scope[id] *= 1000;
          $scope.countTotal();
          return document.getElementById(id).focus();
        };
        $scope.add1000000 = function(id) {
          $scope[id] *= 1000000;
          $scope.countTotal();
          return document.getElementById(id).focus();
        };
        $scope.deleteData = function() {
          $scope["delete"] = true;
          return $scope.postData();
        };
        $scope.changeCode = function() {
          $scope.code = $scope.agent_byid[$scope.agent].code;
          return setTimeout(function() {
            return document.getElementById("code").focus();
          }, 100);
        };
        $scope.open = function($event) {
          $event.preventDefault();
          $event.stopPropagation();
          return $scope.opened = true;
        };
        $scope.dateOptions = {
          'year-format': "'yy'",
          'starting-day': 1
        };
        $scope.addProduct = function() {
            return $location.path('admin_products');
        }
        $scope.cancelData();
        return getData();
      }
    }
  ]);

}).call(this);
