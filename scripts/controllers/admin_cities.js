(function() {
  app.controller('admin_cities', [
    '$scope', '$rootScope', '$location', 'post', function($scope, $rootScope, $location, post) {
      var getData;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.sort = 'actived';
        $scope.revert = true;
        $scope.cancelData = function(reload) {
          $scope.name = "";
          $scope.edit_id = 0;
          $scope.action = $rootScope.lang.add;
          $scope.cta = $rootScope.lang.add_city;
          $scope.actived = "1";
          if (reload) {
            return getData();
          }
        };
        getData = function() {
          var data;
          data = {
            method: "get",
            from: "cities",
            describe: ["from"]
          };
          return post.load(data, true).then(function(data) {
            var item, _i, _len, _ref, _results;
            $rootScope.city_list = data.data;
            $rootScope.city_byid = [];
            _ref = $rootScope.city_list;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              _results.push($scope.city_byid[item.Id] = item);
            }
            return _results;
          });
        };
        $scope.postData = function() {
          var postData;
          postData = {
            method: "post",
            from: "cities",
            describe: ["name", "id", "actived"],
            name: $scope.name,
            id: $scope.edit_id,
            actived: $scope.actived
          };
          $scope.cancelData();
          if ($scope["delete"] === true) {
            postData["delete"] = true;
            postData.describe.push("delete");
          }
          return post.load(postData).then(function(data) {
            $rootScope.city_byid[$scope.edit_id] = postData;
            return $scope.cancelData(true);
          });
        };
        $scope.editData = function(id) {
          $scope.action = $rootScope.lang.edit;
          $scope.cta = $rootScope.lang.edit_city;
          $scope.edit_id = $rootScope.city_byid[id].Id;
          $scope.name = $rootScope.city_byid[id].name;
          $scope.actived = $rootScope.city_byid[id].actived;
          return document.getElementById("first").focus();
        };
        $scope.deleteData = function() {
          $scope["delete"] = true;
          return $scope.postData;
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
        return $scope.cancelData(true);
      }
    }
  ]);

}).call(this);
