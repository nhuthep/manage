(function() {
  app.controller('storage_inout', [
    '$scope', '$rootScope', '$location', 'post', function($scope, $rootScope, $location, post) {
      var dt, _i, _ref, _ref1, _results;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.storage_list = [];
        $scope.sort = "date";
        $scope.revert = false;
        $scope.getOrder = function() {
          var data;
          if (!$rootScope.order_list) {
            data = {
              method: "get",
              from: "orders",
              describe: ["from"]
            };
            return post.load(data, true).then(function(data) {
              var item, _i, _len, _ref;
              $rootScope.order_list = data.data;
              $rootScope.order_byid = [];
              _ref = $rootScope.order_list;
              for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                item = _ref[_i];
                $rootScope.order_byid[item.Id] = item;
              }
              return $scope.getStorage();
            });
          } else {
            return $scope.getStorage();
          }
        };
        $scope.getStorage = function() {
          var data, dt, ndt, next_month, next_year, where;
          dt = $scope.year + "-" + $scope.month + "-1";
          if ($scope.month === 12) {
            next_month = 1;
            next_year = $scope.year + 1;
          } else {
            next_month = $scope.month + 1;
            next_year = $scope.year;
          }
          ndt = next_year + "-" + next_month + "-1";
          where = '`date` > "' + dt + '" AND `date` < "' + ndt + '"';
          data = {
            method: "get",
            from: "depot",
            where: where,
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            var item, _i, _len, _ref, _results;
            $scope.storage_list = data.data;
            $scope.storage_byid = [];
            _ref = $scope.storage_list;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              item.price = Math.floor(item.amount / item.quantity);
              if ($rootScope.depot_type_byid[item.depot_type]) {
                item.depot_type_name = $rootScope.depot_type_byid[item.depot_type].name;
              }
              if ($rootScope.order_byid[item.order]) {
                item.order_code = $rootScope.order_byid[item.order].code;
              }
              _results.push($scope.storage_byid[item.Id] = item);
            }
            return _results;
          });
        };
        $scope.editData = function(id, inoutid) {
          if ($scope.storage_byid[id].inout === "1") {
            return $location.path("action_storage/" + id);
          } else {
            return $location.path("action_inout/" + inoutid);
          }
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
        $scope.month_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        dt = new Date();
        $scope.month = dt.getMonth();
        $scope.month += 1;
        $scope.year = dt.getFullYear();
        $scope.year_list = (function() {
          _results = [];
          for (var _i = _ref = $scope.start_year, _ref1 = $scope.year; _ref <= _ref1 ? _i <= _ref1 : _i >= _ref1; _ref <= _ref1 ? _i++ : _i--){ _results.push(_i); }
          return _results;
        }).apply(this);
        return $scope.getOrder();
      }
    }
  ]);

}).call(this);
