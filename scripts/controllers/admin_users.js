(function() {
  app.controller('admin_users', [
    '$scope', '$rootScope', '$location', 'post', function($scope, $rootScope, $location, post) {
      var getData;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.sort = 'username';
        $scope.revert = false;
        $scope.cancelData = function(reload) {
          $scope.username = "";
          $scope.password = "";
          $scope.realname = "";
          $scope.comment = "";
          $scope.edit_id = 0;
          $scope.action = $rootScope.lang.add;
          $scope.cta = $rootScope.lang.add_user;
          $scope.actived = "1";
          if (reload) {
            return getData();
          }
        };
        getData = function() {
          var data;
          data = {
            method: "get",
            from: "users",
            describe: ["from"]
          };
          return post.load(data, true).then(function(data) {
            var item, _i, _len, _ref, _results;
            $rootScope.user_list = data.data;
            $rootScope.user_byid = [];
            _ref = $rootScope.user_list;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              item.type_name = $rootScope.user_types[item.type];
              _results.push($rootScope.user_byid[item.Id] = item);
            }
            return _results;
          });
        };
        $scope.postData = function() {
          var postData;
          postData = {
            method: "post",
            from: "users",
            describe: ["id", "username", "password", "realname", "type", "comment", "actived"],
            id: $scope.edit_id,
            username: $scope.username,
            password: $scope.password,
            realname: $scope.realname,
            type: $scope.type,
            comment: $scope.comment,
            actived: $scope.actived
          };
          $scope.cancelData();
          if ($scope["delete"] === true) {
            postData["delete"] = true;
            postData.describe.push("delete");
          }
          return post.load(postData).then(function(data) {
            $rootScope.user_byid[$scope.edit_id] = postData;
            return $scope.cancelData(true);
          });
        };
        $scope.editData = function(id) {
          $scope.action = $rootScope.lang.edit;
          $scope.cta = $rootScope.lang.edit_user;
          $scope.edit_id = $rootScope.user_byid[id].Id;
          $scope.username = $rootScope.user_byid[id].username;
          $scope.password = $rootScope.user_byid[id].password;
          $scope.realname = $rootScope.user_byid[id].realname;
          $scope.type = $rootScope.user_byid[id].type;
          $scope.comment = $rootScope.user_byid[id].comment;
          $scope.actived = $rootScope.user_byid[id].actived;
          return document.getElementById("first").focus();
        };
        $scope.deleteData = function() {
          $scope["delete"] = true;
          return $scope.postData;
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
        return $scope.cancelData(true);
      }
    }
  ]);

}).call(this);
