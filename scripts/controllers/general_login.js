(function() {
  app.controller('general_login', [
    '$scope', '$rootScope', '$location', '$http', 'urls', 'login', function($scope, $rootScope, $location, $http, urls, login) {
      $scope.login = function() {
        return login.login($scope.username, $scope.password, $scope.company_code);
      };
      return document.getElementById("first").focus();
    }
  ]);

}).call(this);
