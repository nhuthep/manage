(function() {
  app.controller('admin_units', [
    '$scope', '$rootScope', '$location', 'post', function($scope, $rootScope, $location, post) {
      var getData;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.sort = 'name';
        $scope.revert = true;
        $scope.cancelData = function(reload) {
          $scope.name = "";
          $scope.actived = "1";
          $scope.edit_id = 0;
          $scope.action = "Thêm";
          $scope.cta = "Thêm loại đơn vị tính";
          if (reload) {
            return getData();
          }
        };
        getData = function() {
          var data;
          data = {
            method: "get",
            from: "units",
            describe: ["from"]
          };
          return post.load(data, true).then(function(data) {
            var item, _i, _len, _ref, _results;
            $scope.units = data.data;
            $scope.unit_byid = [];
            _ref = $scope.units;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              _results.push($scope.unit_byid[item.Id] = item);
            }
            return _results;
          });
        };
        $scope.postData = function() {
          var postData;
          postData = {
            method: "post",
            from: "units",
            describe: ["name", "id", "actived"],
            name: $scope.name,
            id: $scope.edit_id,
            actived: $scope.actived
          };
          if ($scope["delete"] === true) {
            postData["delete"] = true;
            postData.describe.push("delete");
          }
          return post.load(postData).then(function(data) {
            return $scope.cancelData(true);
          });
        };
        $scope.editData = function(id) {
          $scope.action = "Sửa";
          $scope.cta = "Sửa tên loại đơn vị tính";
          $scope.edit_id = $scope.unit_byid[id].Id;
          $scope.name = $scope.unit_byid[id].name;
          $scope.actived = $scope.unit_byid[id].actived;
          return document.getElementById("first").focus();
        };
        $scope.deleteData = function() {
          $scope["delete"] = true;
          return $scope.postData;
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
        return $scope.cancelData(true);
      }
    }
  ]);

}).call(this);
