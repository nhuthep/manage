(function() {
  app.controller('employee_list', [
    '$scope', '$rootScope', '$location', function($scope, $rootScope, $location) {
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.sort = 'salary';
        $scope.revert = true;
        $scope.editData = function(id) {
          return $location.path("employee_profile/" + id);
        };
        $scope.addData = function() {
          return $location.path("employee_profile/0");
        };
        return $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
      }
    }
  ]);

}).call(this);
