(function() {
  app.controller('action_share', [
    '$scope', '$rootScope', '$routeParams', '$location', 'post', '$http', 'urls', function($scope, $rootScope, $routeParams, $location, post, $http, urls) {
      var dt, getData, _i, _ref, _ref1, _results;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.edit_id = 0;
        $scope.agent = 0;
        $scope.pushedData = [];
        $scope.cancelData = function(stop) {
          $scope.amount = 0;
          $scope.share_type = 0;
          $scope.comment = "";
          $scope.user = $rootScope.userrealname;
          if (!stop) {
            $scope.edit_id = $routeParams.id;
            return getData();
          } else {
            return $scope.edit_id = 0;
          }
        };
        getData = function() {
          var data;
          if ($scope.edit_id !== "0") {
            data = {
              method: "get",
              from: "shared_outcome",
              where: "`Id` = " + $scope.edit_id,
              describe: ["from", "where"]
            };
            return post.load(data, true).then(function(data) {
              var edit_data;
              edit_data = data.data[0];
              $scope.amount = parseInt(edit_data.amount);
              $scope.share_type = edit_data.share_type;
              $scope.agent = edit_data.agent;
              $scope.month = parseInt(edit_data.month);
              $scope.year = parseInt(edit_data.year);
              $scope.comment = edit_data.comment;
              return $scope.user = edit_data.user;
            });
          }
        };
        $scope.postData = function() {
          var postData;
          postData = {
            method: "post",
            from: "shared_outcome",
            describe: ["id", "amount", "share_type", "agent", "month", "year", "comment", "user"],
            id: $scope.edit_id,
            amount: $scope.amount,
            share_type: $scope.share_type,
            agent: $scope.agent,
            month: $scope.month,
            year: $scope.year,
            comment: $scope.comment,
            user: $rootScope.userrealname
          };
          if ($scope["delete"] === true) {
            postData["delete"] = true;
            postData.describe.push("delete");
          }
          return post.load(postData, true).then(function(data) {
            $scope.pushedData.push(postData);
            $scope.cancelData(true);
            return $http.get(urls.share_update).success(function(data) {});
          });
        };
        $scope.add1000 = function() {
          $scope.amount *= 1000;
          return document.getElementById("amount").focus();
        };
        $scope.add1000000 = function() {
          $scope.amount *= 1000000;
          return document.getElementById("amount").focus();
        };
        $scope.deleteData = function() {
          $scope["delete"] = true;
          return $scope.postData();
        };
        $scope.month_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        dt = new Date();
        $scope.month = dt.getMonth();
        $scope.month += 1;
        $scope.year = dt.getFullYear();
        $scope.year_list = (function() {
          _results = [];
          for (var _i = _ref = $scope.start_year, _ref1 = $scope.year; _ref <= _ref1 ? _i <= _ref1 : _i >= _ref1; _ref <= _ref1 ? _i++ : _i--){ _results.push(_i); }
          return _results;
        }).apply(this);
        return $scope.cancelData();
      }
    }
  ]);

}).call(this);
