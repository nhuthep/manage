(function() {
  app.controller('task_daily', [
    '$scope', '$rootScope', '$location', 'post', 'date', function($scope, $rootScope, $location, post, date) {
      var getData;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.i = 0;
        $scope.task_list = [];
        $scope.getOrder = function() {
          var data, dt, where;
          $scope.task_list = [];
          dt = date.yyyymmdd($scope.date);
          where = '`start_date` <= "' + dt + '" AND (`closed` >= "' + dt + '" OR `closed` = "0000-00-00")';
          $scope.order_byid = [];
          data = {
            method: "get",
            from: "orders",
            where: where,
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            var item, _i, _len, _ref;
            $scope.order_list = data.data;
            _ref = $scope.order_list;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              item.active = 1;
              item.index = 0;
              item.hours = 0;
              item.comment = "";
              $scope.order_byid[item.Id] = item;
            }
            return getData(dt);
          });
        };
        getData = function(dt) {
          var data, where;
          where = '`date` = "' + dt + '"';
          data = {
            method: "get",
            from: "daily_tasks",
            where: where,
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            var item, total_hours, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _ref2, _results;
            _ref = data.data;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              if ($scope.order_byid[item.order]) {
                $scope.order_byid[item.order].hours = item.hours;
                $scope.order_byid[item.order].comment = item.comment;
                $scope.order_byid[item.order].index = item.Id;
              }
            }
            total_hours = 0;
            _ref1 = $scope.order_list;
            for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
              item = _ref1[_j];
              if ($scope.order_byid[item.Id].hours) {
                item.index = $scope.order_byid[item.Id].index;
                item.hours = parseInt($scope.order_byid[item.Id].hours);
                total_hours += item.hours;
                item.comment = $scope.order_byid[item.Id].comment;
              } else {
                item.index = 0;
                item.hours = 0;
                item.comment = "";
              }
            }
            $scope.task_list = [];
            _ref2 = $scope.order_list;
            _results = [];
            for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
              item = _ref2[_k];
              item.col = item.hours / total_hours * 100;
              if (item.hours > 0) {
                _results.push($scope.task_list.push(item));
              } else {
                _results.push(void 0);
              }
            }
            return _results;
          });
        };
        $scope.postData = function() {
          var currentHours, postData;
          if ($scope.i < $scope.order_list.length) {
            postData = {
              method: "post",
              from: "daily_tasks",
              describe: ["id", "date", "order", "hours", "comment"],
              id: $scope.order_list[$scope.i].index,
              date: $scope.date,
              order: $scope.order_list[$scope.i].Id,
              hours: $scope.order_list[$scope.i].hours,
              comment: $scope.order_list[$scope.i].comment
            };
            currentHours = parseInt($scope.order_list[$scope.i].hours);
            if (currentHours === 0 && postData.id > 0) {
              postData["delete"] = true;
              postData.describe.push("delete");
            }
            if (postData["delete"] === true || currentHours > 0) {
              post.load(postData, true).then(function(data) {});
            }
            $scope.i += 1;
            return $scope.postData();
          } else {
            return $scope.i = 0;
          }
        };
        $scope.open = function($event) {
          $event.preventDefault();
          $event.stopPropagation();
          return $scope.opened = true;
        };
        $scope.dateOptions = {
          'year-format': "'yy'",
          'starting-day': 1
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
        if ($rootScope.date) {
          $scope.date = $rootScope.date;
        } else {
          $scope.date = date.thisdate();
        }
        return $scope.getOrder();
      }
    }
  ]);

}).call(this);
