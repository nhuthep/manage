(function() {
  app.controller('finance_partner', [
    '$scope', '$rootScope', '$location', 'post', function($scope, $rootScope, $location, post) {
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.inputs = [];
        $scope.getInput = function(partner_id) {
          var data, where;
          if (partner_id) {
            $scope.partner = partner_id;
          }
          $scope.total_debt = 0;
          $scope.total_in = 0;
          $scope.total_out = 0;
          where = '`partner` = ' + $scope.partner;
          data = {
            method: "get",
            from: "input",
            where: where,
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            var item, _i, _len, _ref;
            $scope.inputs = [];
            _ref = data.data;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              item.active = 1;
              if (item.inout === "2") {
                $scope.total_in += parseInt(item.amount);
              }
              if (item.inout === "1") {
                $scope.total_out += parseInt(item.amount);
              }
              $scope.inputs[item.Id] = item;
            }
            return $scope.total_debt = $scope.total_in - $scope.total_out;
          });
        };
        $scope.editData = function(id) {
          return $location.path("action_inout/" + id);
        };
        $scope.addData = function() {
          $rootScope.partner = $scope.partner;
          $rootScope.inout = 1;
          return $location.path("action_inout/0");
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
        return $scope.showChoice = function() {
          return $scope.partner = void 0;
        };
      }
    }
  ]);

}).call(this);
