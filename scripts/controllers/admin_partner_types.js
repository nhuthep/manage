(function() {
  app.controller('admin_partner_types', [
    '$scope', '$rootScope', '$location', 'post', function($scope, $rootScope, $location, post) {
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.sort = 'name';
        $scope.revert = true;
        $scope.cancelData = function(reload) {
          $scope.name = "";
          $scope.actived = "1";
          $scope.edit_id = 0;
          $scope.action = $rootScope.lang.add;
          $scope.cta = $rootScope.lang.add_partner_type;
          if (reload) {
            return $scope.getData();
          }
        };
        $scope.getData = function() {
          var data;
          data = {
            method: "get",
            from: "partner_types",
            describe: ["from"]
          };
          return post.load(data, true).then(function(data) {
            var item, _i, _len, _ref, _results;
            $rootScope.partner_type_list = data.data;
            $rootScope.partner_type_byid = [];
            _ref = $rootScope.partner_type_list;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              _results.push($rootScope.partner_type_byid[item.Id] = item);
            }
            return _results;
          });
        };
        $scope.postData = function() {
          var postData;
          postData = {
            method: "post",
            from: "partner_types",
            describe: ["id", "name", "actived"],
            name: $scope.name,
            id: $scope.edit_id,
            actived: $scope.actived
          };
          if ($scope["delete"] === true) {
            postData["delete"] = true;
            postData.describe.push("delete");
          }
          return post.load(postData).then(function(data) {
            $rootScope.partner_type_byid[$scope.edit_id] = postData;
            return $scope.cancelData(true);
          });
        };
        $scope.editData = function(id) {
          $scope.action = $rootScope.lang.edit;
          $scope.cta = $rootScope.lang.edit_partner_type;
          $scope.edit_id = $rootScope.partner_type_byid[id].Id;
          $scope.name = $rootScope.partner_type_byid[id].name;
          $scope.actived = $rootScope.partner_type_byid[id].actived;
          $("html, body").animate({
            scrollTop: 0
          }, 500);
          return document.getElementById("first").focus();
        };
        $scope.deleteData = function() {
          $scope["delete"] = true;
          return $scope.postData();
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };

        $scope.cancelData();
        return $scope.getData();
      }
    }
  ]);

}).call(this);
