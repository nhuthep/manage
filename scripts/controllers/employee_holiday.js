(function() {
  app.controller('employee_holiday', [
    '$scope', '$rootScope', '$location', 'post', 'date', function($scope, $rootScope, $location, post, date) {
      var dt, getData, _i, _ref, _ref1, _results;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.view_type = 0;
        $scope.getDate = function() {
          var item, _i, _len, _ref;
          $scope.date_list = date.getDaysArray($scope.month, $scope.year);
          $scope.date_byid = {};
          _ref = $scope.date_list;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            item = _ref[_i];
            item.Id = 0;
            $scope.date_byid[item.fulldate] = item;
            if (item.dow === 0) {
              item.type = 2;
            } else {
              item.type = 0;
            }
          }
          return getData();
        };
        getData = function() {
          var data, month, nextmonth, nextyear, where;
          if ($scope.month === 12) {
            nextmonth = 1;
            nextyear = $scope.year + 1;
          } else {
            nextmonth = $scope.month + 1;
            nextyear = $scope.year;
          }
          if ($scope.month < 10) {
            month = '0' + $scope.month;
          } else {
            month = $scope.month;
          }
          if (nextmonth < 10) {
            nextmonth = '0' + nextmonth;
          }
          where = '`date` >= "' + $scope.year + '-' + month + '-01" AND `date` < "' + nextyear + '-' + nextmonth + '-01"';
          data = {
            method: "get",
            where: where,
            from: "public_holiday",
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            var item, _i, _len, _ref, _results;
            _ref = data.data;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              $scope.date_byid[item.date].type = item.type;
              $scope.date_byid[item.date].Id = item.Id;
              _results.push($scope.date_list[$scope.date_byid[item.date].date - 1].Id = item.Id);
            }
            return _results;
          });
        };
        $scope.postData = function(item) {
          var postData;
          if (item.type !== 2) {
            postData = {
              method: "post",
              from: "public_holiday",
              describe: ["id", "date", "type"],
              id: item.Id,
              date: item.fulldate,
              type: 1
            };
            if (parseInt(item.type) === 0) {
              postData["delete"] = true;
              postData.describe.push("delete");
            }
            return post.load(postData, true).then(function(data) {
              return getData();
            });
          }
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
        $scope.month_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        dt = new Date();
        $scope.month = dt.getMonth();
        $scope.month += 1;
        $scope.year = dt.getFullYear();
        $scope.year_list = (function() {
          _results = [];
          for (var _i = _ref = $scope.start_year, _ref1 = $scope.year; _ref <= _ref1 ? _i <= _ref1 : _i >= _ref1; _ref <= _ref1 ? _i++ : _i--){ _results.push(_i); }
          return _results;
        }).apply(this);
        return $scope.getDate();
      }
    }
  ]);

}).call(this);
