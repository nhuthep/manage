(function() {
  app.controller('storage_item', [
    '$scope', '$rootScope', '$location', 'post', function($scope, $rootScope, $location, post) {
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.storage_list = [];
        $scope.sort = "time_order";
        $scope.revert = false;
        $scope.getOrder = function() {
          var data;
          if (!$rootScope.order_list) {
            data = {
              method: "get",
              from: "orders",
              describe: ["from"]
            };
            return post.load(data, true).then(function(data) {
              var item, _i, _len, _ref;
              $rootScope.order_list = data.data;
              $rootScope.order_byid = [];
              _ref = $rootScope.order_list;
              for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                item = _ref[_i];
                $rootScope.order_byid[item.Id] = item;
              }
              return $scope.getStorage();
            });
          } else {
            return $scope.getStorage();
          }
        };
        $scope.getStorage = function(depot_type_id) {
          var data, where;
          if (depot_type_id) {
            $scope.depot_type = depot_type_id;
          }
          if ($scope.depot_type > 0) {
            where = '`depot_type` = ' + $scope.depot_type;
            data = {
              method: "get",
              from: "depot",
              where: where,
              describe: ["from", "where"]
            };
            return post.load(data, true).then(function(data) {
              var item, _i, _len, _ref;
              $scope.storage_list = data.data;
              $scope.storage_byid = [];
              $scope.total = 0;
              _ref = $scope.storage_list;
              for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                item = _ref[_i];
                item.time_order = parseInt(item.time_order);
                item.price = Math.floor(item.amount / item.quantity);
                if ($rootScope.order_byid[item.order]) {
                  item.order_code = $rootScope.order_byid[item.order].code;
                } else {
                  item.order_code = "";
                }
                $scope.storage_byid[item.Id] = item;
              }
              $scope.total_in = $rootScope.depot_type_byid[$scope.depot_type].total_in;
              $scope.total_out = $rootScope.depot_type_byid[$scope.depot_type].total_out;
              return $scope.total = $scope.total_in - $scope.total_out;
            });
          }
        };
        $scope.editData = function(id, inoutid) {
          if ($scope.storage_byid[id].inout === "1") {
            return $location.path("action_storage/" + id);
          } else {
            return $location.path("action_inout/" + inoutid);
          }
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
        return $scope.getOrder();
      }
    }
  ]);

}).call(this);
