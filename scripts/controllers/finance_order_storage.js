(function() {
  app.controller('finance_order_storage', [
    '$scope', '$rootScope', '$routeParams', '$location', 'post', function($scope, $rootScope, $routeParams, $location, post) {
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.storage_list = [];
        $scope.sort = "time_order";
        $scope.revert = false;
        $scope.getStorage = function() {
          var data, where;
          where = '`order` = ' + $routeParams.id;
          data = {
            method: "get",
            from: "depot",
            where: where,
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            var item, _i, _len, _ref, _results;
            $scope.storage_list = data.data;
            $scope.storage_byid = [];
            $scope.total = 0;
            _ref = $scope.storage_list;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              item.depot_type_name = $rootScope.depot_type_byid[item.depot_type].name;
              item.time_order = parseInt(item.time_order);
              item.price = Math.floor(item.amount / item.quantity);
              $scope.total += parseInt(item.amount);
              _results.push($scope.storage_byid[item.Id] = item);
            }
            return _results;
          });
        };
        $scope.editData = function(id, inoutid) {
          if ($scope.storage_byid[id].inout === "1") {
            return $location.path("action_storage/" + id);
          } else {
            return $location.path("action_inout/" + inoutid);
          }
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
        return $scope.getStorage();
      }
    }
  ]);

}).call(this);
