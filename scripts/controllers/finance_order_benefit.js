(function() {
  app.controller('finance_order_benefit', [
    '$scope', '$rootScope', '$location', 'post', 'date', function($scope, $rootScope, $location, post, date) {
      var dt, getData, getOrder, _i, _ref, _ref1, _results;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.sort = "start_date";
        $scope.revert = false;
        $scope.total = [];
        getOrder = function() {
          var data;
          data = {
            method: "get",
            from: "orders",
            describe: ["from"]
          };
          return post.load(data, true).then(function(data) {
            var item, _i, _len, _ref;
            $scope.order_list = data.data;
            $scope.order_byid = [];
            _ref = $scope.order_list;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              $scope.order_byid[item.Id] = item;
            }
            return getData();
          });
        };
        getData = function() {
          var data;
          data = {
            method: "get",
            from: "order_value",
            describe: ["from"]
          };
          return post.load(data, true).then(function(data) {
            var item, order, _i, _len, _ref, _results;
            _ref = $scope.order_list;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              if (item.card > -1) {
                item.card_name = $scope.card_byid[item.card].ten;
              } else {
                item.card_name = "@ Thiết kế riêng";
              }
              _results.push((function() {
                var _j, _len1, _ref1, _results1;
                _ref1 = data.data;
                _results1 = [];
                for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
                  order = _ref1[_j];
                  if (item.Id === order.order) {
                    item.date_str = item.start_date.split("-");
                    item.month = parseInt(item.date_str[1]);
                    item.year = parseInt(item.date_str[0]);
                    if (!$scope.total[item.month]) {
                      $scope.total[item.month] = [];
                    }
                    if (!$scope.total[item.month][item.year]) {
                      $scope.total[item.month][item.year] = {
                        amount: 0,
                        salary_value: 0,
                        depot_value: 0,
                        share_value: 0,
                        agent_value: 0,
                        total_benefit: 0
                      };
                    }
                    item.amount = order.amount;
                    item.salary_value = order.salary_value;
                    item.depot_value = order.depot_value;
                    item.share_value = order.share_value;
                    item.agent_value = order.agent_value;
                    item.total_benefit = order.total_benefit;
                    item.benefit_percent = Math.floor((item.total_benefit / item.amount * 1000) / 10);
                    $scope.total[item.month][item.year].amount += parseInt(order.amount);
                    $scope.total[item.month][item.year].salary_value += parseInt(order.salary_value);
                    $scope.total[item.month][item.year].depot_value += parseInt(order.depot_value);
                    $scope.total[item.month][item.year].share_value += parseInt(order.share_value);
                    $scope.total[item.month][item.year].agent_value += parseInt(order.agent_value);
                    $scope.total[item.month][item.year].total_benefit += parseInt(order.total_benefit);
                    $scope.total[item.month][item.year].benefit_percent = Math.floor(($scope.total[item.month][item.year].total_benefit / $scope.total[item.month][item.year].amount * 1000) / 10);
                    $scope.total[item.month][item.year].salary_percent = Math.floor(($scope.total[item.month][item.year].salary_value / $scope.total[item.month][item.year].amount * 1000) / 10);
                    $scope.total[item.month][item.year].depot_percent = Math.floor(($scope.total[item.month][item.year].depot_value / $scope.total[item.month][item.year].amount * 1000) / 10);
                    $scope.total[item.month][item.year].share_percent = Math.floor(($scope.total[item.month][item.year].share_value / $scope.total[item.month][item.year].amount * 1000) / 10);
                    $scope.total[item.month][item.year].agent_percent = Math.floor(($scope.total[item.month][item.year].agent_value / $scope.total[item.month][item.year].amount * 1000) / 10);
                    if (item.benefit_percent < 0) {
                      item.level = 1;
                    }
                    if (item.benefit_percent >= 0 && item.benefit_percent < 10) {
                      item.level = 2;
                    }
                    if (item.benefit_percent >= 10 && item.benefit_percent < 30) {
                      item.level = 3;
                    }
                    if (item.benefit_percent >= 30) {
                      _results1.push(item.level = 4);
                    } else {
                      _results1.push(void 0);
                    }
                  } else {
                    _results1.push(void 0);
                  }
                }
                return _results1;
              })());
            }
            return _results;
          });
        };
        $scope.editData = function(id) {
          return $location.path("action_order/" + id);
        };
        $scope.viewStorage = function(id) {
          return $location.path("finance_order_storage/" + id);
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
        $scope.month_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        dt = new Date();
        $scope.month = dt.getMonth();
        $scope.month += 1;
        $scope.year = dt.getFullYear();
        $scope.year_list = (function() {
          _results = [];
          for (var _i = _ref = $scope.start_year, _ref1 = $scope.year; _ref <= _ref1 ? _i <= _ref1 : _i >= _ref1; _ref <= _ref1 ? _i++ : _i--){ _results.push(_i); }
          return _results;
        }).apply(this);
        return getOrder();
      }
    }
  ]);

}).call(this);
