(function() {
  app.controller('admin_share_types', [
    '$scope', '$rootScope', '$location', 'post', function($scope, $rootScope, $location, post) {
      var getData;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.sort = 'name';
        $scope.revert = true;
        $scope.cancelData = function(reload) {
          $scope.name = "";
          $scope.actived = "1";
          $scope.edit_id = 0;
          $scope.action = $rootScope.lang.add;
          $scope.cta = $rootScope.lang.add_share_type;
          if (reload) {
            return getData();
          }
        };
        getData = function() {
          var data;
          data = {
            method: "get",
            from: "share_types",
            describe: ["from"]
          };
          return post.load(data, true).then(function(data) {
            var item, _i, _len, _ref, _results;
            $scope.share_types = data.data;
            $scope.share_type_byid = [];
            _ref = $scope.share_types;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              _results.push($scope.share_type_byid[item.Id] = item);
            }
            return _results;
          });
        };
        $scope.postData = function() {
          var postData;
          postData = {
            method: "post",
            from: "share_types",
            describe: ["name", "id", "actived"],
            name: $scope.name,
            id: $scope.edit_id,
            actived: $scope.actived
          };
          if ($scope["delete"] === true) {
            postData["delete"] = true;
            postData.describe.push("delete");
          }
          return post.load(postData).then(function(data) {
            return $scope.cancelData(true);
          });
        };
        $scope.editData = function(id) {
          $scope.action = $rootScope.lang.edit;
          $scope.cta = $rootScope.lang.edit_share_type;
          $scope.edit_id = $rootScope.share_type_byid[id].Id;
          $scope.name = $rootScope.share_type_byid[id].name;
          $scope.actived = $rootScope.share_type_byid[id].actived;
          return document.getElementById("first").focus();
        };
        $scope.deleteData = function() {
          $scope["delete"] = true;
          return $scope.postData();
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
        return $scope.cancelData(true);
      }
    }
  ]);

}).call(this);
