(function() {
  app.controller('action_storage', [
    '$scope', '$rootScope', '$routeParams', '$location', 'post', 'date', '$http', 'urls', function($scope, $rootScope, $routeParams, $location, post, date, $http, urls) {
      var getData, getOrders;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.edit_id = 0;
        $scope.cancelData = function(stop) {
          $scope.order = "";
          $scope.cost_type = "";
          $scope.depot_type = "";
          $scope.quantity = "";
          $scope.comment = "";
          $scope.action = "Thêm";
          $scope.cta = "Xuất kho";
          $scope.date = date.thisdate();
          $scope.user = $rootScope.userrealname;
          if (!stop) {
            $scope.edit_id = $routeParams.id;
            return getOrders();
          } else {
            return $scope.edit_id = 0;
          }
        };
        getOrders = function() {
          var data;
          data = {
            method: "get",
            from: "orders",
            where: "`closed` = '0000-00-00'",
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            $scope.order_list = data.data;
            return getData();
          });
        };
        getData = function() {
          var data;
          if ($scope.edit_id !== "0") {
            data = {
              method: "get",
              from: "depot",
              where: "`Id` = " + $scope.edit_id,
              describe: ["from", "where"]
            };
            return post.load(data, true).then(function(data) {
              var edit_data;
              edit_data = data.data[0];
              $scope.order = edit_data.order;
              $scope.date = edit_data.date;
              $scope.depot_type = edit_data.depot_type;
              $scope.cost_type = $scope.depot_type_byid[$scope.depot_type].cost_type;
              $scope.quantity = parseFloat(edit_data.quantity);
              $scope.comment = edit_data.comment;
              $scope.user = edit_data.user;
              $scope.action = "Sửa / Xóa";
              return $scope.cta = "Sửa / Xóa thông tin xuất kho";
            });
          } else {
            if ($rootScope.depot_type > 0) {
              $scope.depot_type = $rootScope.depot_type;
              $scope.cost_type = $scope.depot_type_byid[$scope.depot_type].cost_type;
              return $rootScope.depot_type = 0;
            }
          }
        };
        $scope.postData = function() {
          var postData;
          postData = {
            method: "post",
            from: "depot",
            describe: ["id", "inout", "order", "depot_type", "quantity", "comment", "user", "date"],
            id: $scope.edit_id,
            inout: 1,
            order: $scope.order,
            depot_type: $scope.depot_type,
            quantity: $scope.quantity,
            user: $rootScope.userrealname,
            comment: $scope.comment,
            date: $scope.date
          };
          if ($scope["delete"] === true) {
            postData["delete"] = true;
            postData.describe.push("delete");
          }
          return post.load(postData, true).then(function(data) {
            $http.get(urls.depot).success(function(data) {});
            $http.get(urls.depot_update).success(function(data) {});
            return $scope.cancelData(true);
          });
        };
        $scope.deleteData = function() {
          $scope["delete"] = true;
          return $scope.postData();
        };
        $scope.open = function($event) {
          $event.preventDefault();
          $event.stopPropagation();
          return $scope.opened = true;
        };
        $scope.dateOptions = {
          'year-format': "'yy'",
          'starting-day': 1
        };
        return $scope.cancelData();
      }
    }
  ]);

}).call(this);
