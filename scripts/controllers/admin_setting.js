/**
 * Created by nhuthep on 9/28/15.
 */
(function() {
    app.controller('admin_setting', [
        '$scope', '$rootScope', '$location', 'post', function($scope, $rootScope, $location, post) {
            if (!$rootScope.username) {
                return $location.path("general_login");
            } else if (!$rootScope.data_loaded) {
                return $location.path("general_home");
            } else {
                $scope.sort = "name";
                $scope.revert = false;
                $rootScope.sub_menu_list = [];

                $scope.getSubMenu = function(itemId, data) {
                    var item, _i, _len, _ref;
                    _ref = data;
                    $scope.sub_menu = [];
                    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                        item = _ref[_i];
                        if (parseInt(item.parent_id) == itemId) {
                            $scope.sub_menu.push(item);
                        }
                    }
                    if ($scope.sub_menu.length > 0) {
                        $rootScope.sub_menu_list[itemId] = $scope.sub_menu;
                    }
                };

                $scope.getMenu = function() {
                    var data;
                    data = {
                        method: "get",
                        from: "menu",
                        describe: ["from"]
                    };
                    return post.load(data, true).then(function(data) {
                        var item, _i, _len, _ref;
                        _ref = data.data;
                        $rootScope.menu_list = [];
                        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                            item = _ref[_i];
                            if (parseInt(item.parent_id) == 0) {
                                $rootScope.menu_list.push(item);
                                $scope.getSubMenu(item.Id, _ref);
                            }
                        }
                        return;
                    });
                };

                $scope.getMenu();
            }
        }
    ]);

}).call(this);
