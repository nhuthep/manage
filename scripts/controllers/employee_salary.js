(function() {
  app.controller('employee_salary', [
    '$scope', '$rootScope', '$location', 'post', 'date', function($scope, $rootScope, $location, post, date) {
      var dt, getData, getDate, getHoliday, getHoursPerDay, getTimesheet, postSalary, setTotalSalary, _i, _ref, _ref1, _results;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.noDays = 0;
        $scope.getTotalMonth = function() {
          var data, where;
          where = '`month` = ' + $scope.month + ' AND `year` = ' + $scope.year;
          data = {
            method: "get",
            from: "total_month",
            where: where,
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            $scope.total_month = data.data[0];
            if (!$scope.total_month) {
              $scope.total_month = {
                Id: 0
              };
            }
            return getTimesheet();
          });
        };
        setTotalSalary = function() {
          var postData;
          postData = {
            method: "post",
            from: "total_month",
            describe: ["id", "total_salary", "month", "year"],
            id: $scope.total_month.Id,
            total_salary: $scope.total_salary,
            month: $scope.month,
            year: $scope.year
          };
          return post.load(postData);
        };
        getDate = function() {
          var item, _i, _len, _ref, _results;
          $scope.date_list = date.getDaysArray($scope.month, $scope.year);
          $scope.date_byid = {};
          _ref = $scope.date_list;
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            item = _ref[_i];
            item.Id = 0;
            $scope.date_byid[item.fulldate] = item;
            if (item.dow === 0) {
              _results.push(item.type = 2);
            } else {
              _results.push(item.type = 0);
            }
          }
          return _results;
        };
        getTimesheet = function() {
          var data, month, nextmonth, nextyear, where;
          getDate();
          if ($scope.month === 12) {
            nextmonth = 1;
            nextyear = $scope.year + 1;
          } else {
            nextmonth = $scope.month + 1;
            nextyear = $scope.year;
          }
          if ($scope.month < 10) {
            month = '0' + $scope.month;
          } else {
            month = $scope.month;
          }
          if (nextmonth < 10) {
            nextmonth = '0' + nextmonth;
          }
          where = '`date` >= "' + $scope.year + '-' + month + '-01" AND `date` < "' + nextyear + '-' + nextmonth + '-01"';
          data = {
            method: "get",
            from: "timesheet",
            where: where,
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            $scope.timesheet_list = data.data;
            return getHoliday(where);
          });
        };
        getHoliday = function(where) {
          var data;
          data = {
            method: "get",
            from: "public_holiday",
            where: where,
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            $scope.holiday_list = data.data;
            return getHoursPerDay();
          });
        };
        getHoursPerDay = function(cover) {
          var data, loaddata;
          if (cover === true) {
            loaddata = false;
          } else {
            loaddata = true;
          }
          data = {
            method: "get",
            from: "staff_month",
            where: '`month` = "' + $scope.month + '" AND `year` = "' + $scope.year + '"',
            describe: ["from", "where"]
          };
          return post.load(data, loaddata).then(function(data) {
            var checkDuplicated, checkItem, item, ret, _i, _j, _len, _len1, _ref;
            $scope.staff_month_list = data.data;
            $scope.staff_month_bystaff = [];
            checkDuplicated = [];
            _ref = $scope.staff_month_list;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              ret = true;
              for (_j = 0, _len1 = checkDuplicated.length; _j < _len1; _j++) {
                checkItem = checkDuplicated[_j];
                if (item.staff === checkItem.staff) {
                  ret = false;
                }
              }
              if (ret) {
                item.salary = parseInt(item.salary);
                item.added = parseInt(item.added);
                item.hours_per_day = parseFloat(item.hours_per_day);
                item.day_off = parseFloat(item.day_off);
                $scope.staff_month_bystaff[item.staff] = item;
                checkDuplicated.push(item);
              }
            }
            $scope.staff_month_list = checkDuplicated;
            return getData();
          });
        };
        getData = function() {
          var item, _i, _j, _k, _l, _len, _len1, _len2, _len3, _len4, _len5, _len6, _m, _n, _o, _ref, _ref1, _ref2, _ref3, _ref4, _ref5, _ref6;
          _ref = $rootScope.staff_list;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            item = _ref[_i];
            item.total_mins = void 0;
          }
          _ref1 = $scope.timesheet_list;
          for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
            item = _ref1[_j];
            if ($rootScope.staff_byid[item.staff].total_mins !== void 0) {
              $rootScope.staff_byid[item.staff].total_mins += parseInt(item.total_mins);
            } else {
              $rootScope.staff_byid[item.staff].total_mins = parseInt(item.total_mins);
            }
            $rootScope.staff_byid[item.staff].real_hours = Math.ceil($rootScope.staff_byid[item.staff].total_mins / 6) / 10;
          }
          _ref2 = $scope.holiday_list;
          for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
            item = _ref2[_k];
            $scope.date_byid[item.date].public_holiday = true;
          }
          $scope.noDays = 0;
          _ref3 = $scope.date_list;
          for (_l = 0, _len3 = _ref3.length; _l < _len3; _l++) {
            item = _ref3[_l];
            if (item.dow !== 0) {
              $scope.noDays += 1;
            }
          }
          _ref4 = $rootScope.staff_list;
          for (_m = 0, _len4 = _ref4.length; _m < _len4; _m++) {
            item = _ref4[_m];
            item.hours_per_day = 0;
            item.staff_month_id = 0;
          }
          _ref5 = $scope.staff_month_list;
          for (_n = 0, _len5 = _ref5.length; _n < _len5; _n++) {
            item = _ref5[_n];
            if ($rootScope.staff_byid[item.staff]) {
              $rootScope.staff_byid[item.staff].staff_month_id = item.Id;
              if (item.hours_per_day > 0) {
                $rootScope.staff_byid[item.staff].hours_per_day = item.hours_per_day;
              }
              if (item.salary > 0) {
                $rootScope.staff_byid[item.staff].salary = item.salary;
              }
            }
          }
          $scope.total_salary = 0;
          _ref6 = $rootScope.staff_list;
          for (_o = 0, _len6 = _ref6.length; _o < _len6; _o++) {
            item = _ref6[_o];
            if (!$scope.staff_month_bystaff[item.Id]) {
              $scope.staff_month_bystaff[item.Id] = {
                day_off: 0,
                salary: 0,
                added: 0,
                hours_per_day: 8
              };
            }
            item.salary = $scope.staff_month_bystaff[item.Id].salary;
            if (!item.real_hours) {
              item.real_hours = 0;
            }
            item.working_hours = item.real_hours;
            item.real_hours += $scope.holiday_list.length * item.hours_per_day;
            item.noDays = $scope.noDays - $scope.staff_month_bystaff[item.Id].day_off - $scope.holiday_list.length;
            item.hourly_salary = item.salary / 208;
            item.overtime = item.real_hours - item.noDays * item.hours_per_day;
            item.noHours = item.noDays * $scope.staff_month_bystaff[item.Id].hours_per_day;
            item.day_off = $scope.staff_month_bystaff[item.Id].day_off;
            item.added = $scope.staff_month_bystaff[item.Id].added;
            if (item.overtime < 0) {
              item.overtime = 0;
              item.total_hours = item.real_hours;
            } else {
              item.total_hours = $scope.noDays * item.hours_per_day + item.overtime * 1.5;
            }
            item.total_salary = item.total_hours * item.hourly_salary + $scope.staff_month_bystaff[item.Id].added;
            postSalary($scope.staff_month_bystaff[item.Id].Id, item.real_hours, item.total_salary);
            $scope.total_salary += item.total_salary;
          }
          return setTotalSalary();
        };
        postSalary = function(id, total_hours, total_salary) {
          var postData;
          postData = {
            method: "post",
            from: "staff_month",
            describe: ["id", "total_hours", "total_salary"],
            id: id,
            total_hours: total_hours,
            total_salary: total_salary
          };
          return post.load(postData);
        };
        $scope.postData = function(staff_month_id, staff, hours_per_day, salary, added, day_off) {
          var postData;
          postData = {
            method: "post",
            from: "staff_month",
            describe: ["id", "staff", "hours_per_day", "month", "year", "salary", "added", "day_off"],
            id: staff_month_id,
            staff: staff,
            hours_per_day: hours_per_day,
            month: $scope.month,
            year: $scope.year,
            salary: salary,
            added: added,
            day_off: day_off
          };
          return post.load(postData, true).then(function(data) {
            return getHoursPerDay(true);
          });
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
        $scope.month_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        dt = new Date();
        $scope.month = dt.getMonth();
        $scope.month += 1;
        $scope.year = dt.getFullYear();
        $scope.year_list = (function() {
          _results = [];
          for (var _i = _ref = $scope.start_year, _ref1 = $scope.year; _ref <= _ref1 ? _i <= _ref1 : _i >= _ref1; _ref <= _ref1 ? _i++ : _i--){ _results.push(_i); }
          return _results;
        }).apply(this);
        return $scope.getTotalMonth();
      }
    }
  ]);

}).call(this);
