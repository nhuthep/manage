(function() {
  app.controller('employee_timesheet_list', [
    '$scope', '$rootScope', '$location', 'post', 'date', function($scope, $rootScope, $location, post, date) {
      var dt, getDate, getHoliday, _i, _ref, _ref1, _results;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        getDate = function() {
          var item, _i, _len, _ref, _results;
          $scope.date_list = date.getDaysArray($scope.month, $scope.year);
          $scope.date_byid = {};
          _ref = $scope.date_list;
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            item = _ref[_i];
            item.Id = 0;
            $scope.date_byid[item.fulldate] = item;
            if (item.dow === 0) {
              _results.push(item.type = 2);
            } else {
              _results.push(item.type = 0);
            }
          }
          return _results;
        };
        $scope.getTimesheet = function() {
          var data, month, nextmonth, nextyear, where;
          getDate();
          if ($scope.month === 12) {
            nextmonth = 1;
            nextyear = $scope.year + 1;
          } else {
            nextmonth = $scope.month + 1;
            nextyear = $scope.year;
          }
          if ($scope.month < 10) {
            month = '0' + $scope.month;
          } else {
            month = $scope.month;
          }
          if (nextmonth < 10) {
            nextmonth = '0' + nextmonth;
          }
          where = '`date` >= "' + $scope.year + '-' + month + '-01" AND `date` < "' + nextyear + '-' + nextmonth + '-01"';
          data = {
            method: "get",
            from: "timesheet",
            where: where,
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            $scope.timesheet_list = data.data;
            return getHoliday(where);
          });
        };
        getHoliday = function(where) {
          var data;
          data = {
            method: "get",
            from: "public_holiday",
            where: where,
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            $scope.holiday_list = data.data;
            return $scope.getData();
          });
        };
        $scope.getData = function() {
          var ae, as, h, item, k, m, me, ms, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _ref2, _results;
          _ref = $scope.date_list;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            item = _ref[_i];
            item.morning_start = void 0;
            item.morning_end = void 0;
            item.afternoon_start = void 0;
            item.afternoon_end = void 0;
            item.add_mins = void 0;
            item.total_mins = void 0;
            item.comment = void 0;
            item.total_hours = void 0;
          }
          _ref1 = $scope.timesheet_list;
          for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
            item = _ref1[_j];
            if ($scope.staff) {
              if (parseInt(item.staff) === parseInt($scope.staff)) {
                ms = item.morning_start.split(":");
                item.morning_start = ms[0] + ':' + ms[1];
                me = item.morning_end.split(":");
                item.morning_end = me[0] + ':' + me[1];
                as = item.afternoon_start.split(":");
                item.afternoon_start = as[0] + ':' + as[1];
                ae = item.afternoon_end.split(":");
                item.afternoon_end = ae[0] + ':' + ae[1];
                for (k in item) {
                  if (k !== "date") {
                    $scope.date_byid[item.date][k] = item[k];
                  }
                }
              }
            } else {
              if ($scope.date_byid[item.date].total_mins !== void 0) {
                $scope.date_byid[item.date].total_mins += parseInt(item.total_mins);
              } else {
                $scope.date_byid[item.date].total_mins = parseInt(item.total_mins);
              }
            }
            if ($scope.date_byid[item.date].total_mins > 0) {
              h = Math.floor($scope.date_byid[item.date].total_mins / 60);
              m = $scope.date_byid[item.date].total_mins - h * 60;
              if (m < 10) {
                m = '0' + m;
              }
              $scope.date_byid[item.date].total_hours = h + ':' + m;
            }
          }
          _ref2 = $scope.holiday_list;
          _results = [];
          for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
            item = _ref2[_k];
            _results.push($scope.date_byid[item.date].public_holiday = true);
          }
          return _results;
        };
        $scope.month_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        dt = new Date();
        $scope.month = dt.getMonth();
        $scope.month += 1;
        $scope.year = dt.getFullYear();
        $scope.year_list = (function() {
          _results = [];
          for (var _i = _ref = $scope.start_year, _ref1 = $scope.year; _ref <= _ref1 ? _i <= _ref1 : _i >= _ref1; _ref <= _ref1 ? _i++ : _i--){ _results.push(_i); }
          return _results;
        }).apply(this);
        return $scope.getTimesheet();
      }
    }
  ]);

}).call(this);
