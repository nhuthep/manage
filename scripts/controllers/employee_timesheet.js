(function() {
  app.controller('employee_timesheet', [
    '$scope', '$rootScope', '$location', 'post', 'date', '$http', 'urls', function($scope, $rootScope, $location, post, date, $http, urls) {
      var cancelData, getData;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.sort = "name";
        $scope.revert = false;
        cancelData = function(nostaff) {
          $scope.Id = 0;
          $scope.edit_id = 0;
          $scope.date = date.yyyymmdd();
          $scope.morning_start = '00:00:00';
          $scope.morning_end = '00:00:00';
          $scope.afternoon_start = '00:00:00';
          $scope.afternoon_end = '00:00:00';
          $scope.add_mins = 0;
          $scope.comment = '';
          if (!nostaff) {
            return $scope.staff = 0;
          }
        };
        getData = function() {
          var data;
          data = {
            method: "get",
            from: "timesheet",
            where: '`date` = "' + $scope.date + '"',
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            var h, item, m, _i, _len, _ref, _results;
            $scope.timesheet_list = data.data;
            $scope.timesheet_byid = [];
            $scope.timesheet_bystaff = [];
            if ($scope.timesheet_list.length > 0) {
              cancelData();
              _ref = $scope.timesheet_list;
              _results = [];
              for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                item = _ref[_i];
                $scope.timesheet_byid[item.id] = item;
                $scope.timesheet_bystaff[item.staff] = item;
                if (item.total_mins > 0) {
                  h = Math.floor(item.total_mins / 60);
                  m = item.total_mins - h * 60;
                  if (m < 10) {
                    m = '0' + m;
                  }
                  _results.push(item.total_hours = h + 'h' + m);
                } else {
                  _results.push(item.total_hours = '0h00');
                }
              }
              return _results;
            }
          });
        };
        $scope.postData = function(type) {
          var ae, afternoon_end, afternoon_start, as, me, morning_end, morning_start, ms, postData;
          switch (type) {
            case 0:
              $scope.morning_start = date.hhmmss();
              break;
            case 1:
              $scope.morning_end = date.hhmmss();
              break;
            case 2:
              $scope.afternoon_start = date.hhmmss();
              break;
            case 3:
              $scope.afternoon_end = date.hhmmss();
          }
          ms = $scope.morning_start.split(":");
          morning_start = parseInt(ms[0] * 60) + parseInt(ms[1]);
          me = $scope.morning_end.split(":");
          morning_end = parseInt(me[0] * 60) + parseInt(me[1]);
          as = $scope.afternoon_start.split(":");
          afternoon_start = parseInt(as[0] * 60) + parseInt(as[1]);
          ae = $scope.afternoon_end.split(":");
          afternoon_end = parseInt(ae[0] * 60) + parseInt(ae[1]);
          $scope.total_mins = parseInt($scope.add_mins);
          if (morning_end > morning_start && morning_start > 0) {
            $scope.total_mins += morning_end - morning_start;
          }
          if (afternoon_end > afternoon_start && afternoon_start > 0) {
            $scope.total_mins += afternoon_end - afternoon_start;
          }
          postData = {
            method: "post",
            from: "timesheet",
            describe: ["id", "date", "morning_start", "morning_end", "afternoon_start", "afternoon_end", "add_mins", "comment", "staff", "total_mins"],
            id: $scope.edit_id,
            date: $scope.date,
            morning_start: $scope.morning_start,
            morning_end: $scope.morning_end,
            afternoon_start: $scope.afternoon_start,
            afternoon_end: $scope.afternoon_end,
            add_mins: $scope.add_mins,
            comment: $scope.comment,
            staff: $scope.staff,
            total_mins: $scope.total_mins
          };
          return post.load(postData, true).then(function(data) {
            getData();
            return $http.get(urls.salary_update).success(function(data) {});
          });
        };
        $scope.editData = function(id) {
          var k;
          $scope.Id = 0;
          if ($scope.timesheet_bystaff[id]) {
            for (k in $scope.timesheet_bystaff[id]) {
              $scope[k] = $scope.timesheet_bystaff[id][k];
            }
            if ($scope.Id > 0) {
              $scope.edit_id = $scope.Id;
              return $scope.add_mins = parseInt($scope.add_mins);
            } else {
              return $scope.edit_id = 0;
            }
          } else {
            return cancelData(true);
          }
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
        cancelData();
        return getData();
      }
    }
  ]);

}).call(this);
