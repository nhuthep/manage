(function() {
  app.controller('storage_summary', [
    '$scope', '$rootScope', '$location', 'post', function($scope, $rootScope, $location, post) {
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.storage_list = [];
        $scope.sort = "name";
        $scope.revert = false;
        $scope.getData = function() {
          var data;
          data = {
            method: "get",
            from: "depot_types",
            describe: ["from"]
          };
          return post.load(data, true).then(function(data) {
            var item, _i, _len, _ref, _results;
            $rootScope.depot_type_list = data.data;
            $scope.total_remain_value = 0;
            _ref = $rootScope.depot_type_list;
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              item.unit_name = "";
              item.cost_type_name = "";
              item.price = 0;
              if ($rootScope.unit_byid[item.unit]) {
                if ($rootScope.unit_byid[item.unit].name) {
                  item.unit_name = $rootScope.unit_byid[item.unit].name;
                }
              }
              if ($rootScope.cost_type_byid[item.cost_type]) {
                if ($rootScope.cost_type_byid[item.cost_type].name) {
                  item.cost_type_name = $rootScope.cost_type_byid[item.cost_type].name;
                }
              }
              $scope.total_remain_value += parseInt(item.remain_value);
              item.remain = parseFloat(item.remain);
              item.remain_value = parseInt(item.remain_value);
              if (item.remain !== 0) {
                _results.push(item.price = Math.floor(item.remain_value / item.remain));
              } else {
                _results.push(void 0);
              }
            }
            return _results;
          });
        };
        $scope.inout = function(inout, id) {
          $rootScope.depot_type = id;
          switch (inout) {
            case 0:
              return $location.path('action_inout/0');
            case 1:
              return $location.path('action_storage/0');
            case 2:
              return $location.path('storage_item');
          }
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
        return $scope.getData();
      }
    }
  ]);

}).call(this);
