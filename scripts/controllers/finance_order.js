(function() {
  app.controller('finance_order', [
    '$scope', '$rootScope', '$location', 'post', 'date', function($scope, $rootScope, $location, post, date) {
      var dt, _i, _ref, _ref1, _results;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.total = 0;
        $scope.total_debt = 0;
        $scope.total_paid = 0;
        $scope.close_list = ["", "x"];
        $scope.order_list = [];
        $scope.partner = 0;
        $scope.getOrder = function() {
          var data, dt, ndt, next_month, next_year, where;
          $scope.total = 0;
          $scope.total_debt = 0;
          $scope.total_paid = 0;
          dt = $scope.year + "-" + $scope.month + "-1";
          if ($scope.month === 12) {
            next_month = 1;
            next_year = $scope.year + 1;
          } else {
            next_month = $scope.month + 1;
            next_year = $scope.year;
          }
          ndt = next_year + "-" + next_month + "-1";
          where = '`start_date` >= "' + dt + '" AND `start_date` < "' + ndt + '"';
          data = {
            method: "get",
            from: "orders",
            where: where,
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            var item, _i, _len, _ref;
            $scope.order_list = data.data;
            $scope.order_byid = [];
            _ref = $scope.order_list;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              item.active = 1;
              if (item.card > -1) {
                item.card_name = $scope.card_byid[item.card].ten;
              } else {
                item.card_name = "@ Thiết kế riêng";
              }
              if (item.closed === "0000-00-00") {
                $scope.total_paid += parseInt(item.deposit);
                $scope.total_debt += parseInt(item.total) - parseInt(item.deposit);
              } else {
                $scope.total_paid += parseInt(item.total);
              }
              $scope.order_byid[item.Id] = item;
            }
            return $scope.total = $scope.total_debt + $scope.total_paid;
          });
        };
        $scope.closeOrder = function(id) {
          var data;
          $scope.order_byid[id].closed = date.yyyymmdd();
          data = {
            method: "post",
            from: "orders",
            describe: ["closed", "id"],
            id: id,
            closed: $scope.order_byid[id].closed
          };
          return post.load(data, true).then(function(data) {});
        };
        $scope.openOrder = function(id) {
          var data;
          $scope.order_byid[id].closed = "0000-00-00";
          data = {
            method: "post",
            from: "orders",
            describe: ["closed", "id"],
            id: id,
            closed: "0000-00-00"
          };
          return post.load(data, true).then(function(data) {});
        };
        $scope.editData = function(id) {
          return $location.path("action_order/" + id);
        };
        $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
        $scope.month_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        dt = new Date();
        $scope.month = dt.getMonth();
        $scope.month += 1;
        $scope.year = dt.getFullYear();
        $scope.year_list = (function() {
          _results = [];
          for (var _i = _ref = $scope.start_year, _ref1 = $scope.year; _ref <= _ref1 ? _i <= _ref1 : _i >= _ref1; _ref <= _ref1 ? _i++ : _i--){ _results.push(_i); }
          return _results;
        }).apply(this);
        return $scope.getOrder();
      }
    }
  ]);

}).call(this);
