(function() {
  app.controller('finance_agent', [
    '$scope', '$rootScope', '$location', 'post', function($scope, $rootScope, $location, post) {
      var getOrders;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.inputs = [];
        $scope.sort = "date";
        $scope.revert = false;
        $scope.getInput = function(partner_id) {
          var data, where;
          if (partner_id) {
            $scope.partner = partner_id;
          }
          $scope.total_debt = 0;
          $scope.total_in = 0;
          $scope.total_out = 0;
          $scope.total_percent = 0;
          where = '`partner` = ' + $scope.partner;
          data = {
            method: "get",
            from: "input",
            where: where,
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            var item, _i, _len, _ref;
            $scope.inputs = data.data;
            $scope.input_byid = [];
            _ref = data.data;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              item.dh = 0;
              item.dId = "TC" + item.Id;
              item.active = 1;
              switch (parseInt(item.inout)) {
                case 0:
                  $scope.total_in += parseInt(item.amount);
                  break;
                case 1:
                  $scope.total_out += parseInt(item.amount);
                  break;
                case 2:
                  $scope.total_percent += parseInt(item.amount);
                  item.code = item.comment;
              }
              $scope.input_byid[item.Id] = item;
            }
            $scope.total_debt = $scope.total_out - $scope.total_in - $scope.total_percent;
            return getOrders();
          });
        };
        getOrders = function() {
          var data;
          data = {
            method: "get",
            from: "orders",
            where: "`agent` = " + $scope.partner,
            describe: ["from", "where"]
          };
          return post.load(data, true).then(function(data) {
            var item, _i, _len, _ref;
            $scope.order_list = data.data;
            _ref = data.data;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              item = _ref[_i];
              if (item.closed !== "0000-00-00") {
                item.total = item.price * item.quantity + parseInt(item.additional);
              } else {
                item.total = item.deposit;
              }
              $scope.inputs.push({
                Id: item.Id,
                dh: 1,
                dId: "DH" + item.Id,
                inout: 1,
                amount: item.total,
                date: item.end_date,
                payment_type: 0,
                payment_code: item.code
              });
              $scope.total_out += parseInt(item.total);
            }
            return $scope.total_debt = $scope.total_out - $scope.total_in - $scope.total_percent;
          });
        };
        $scope.deleteData = function() {
          $scope["delete"] = true;
          return $scope.postData();
        };
        $scope.editData = function(id, type) {
          if (type === 0) {
            return $location.path("action_inout/" + id);
          } else {
            return $location.path("action_order/" + id);
          }
        };
        $scope.addData = function(type) {
          $rootScope.partner = $scope.partner;
          if (type === 0) {
            return $location.path("action_inout/0");
          } else {
            return $location.path("action_order/0");
          }
        };
        return $scope.sortBy = function(field) {
          if ($scope.sort === field) {
            return $scope.revert = !$scope.revert;
          } else {
            $scope.sort = field;
            return $scope.revert = false;
          }
        };
      }
    }
  ]);

}).call(this);
