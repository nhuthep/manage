(function() {
  app.controller('action_inout', [
    '$scope', '$rootScope', '$routeParams', '$location', 'post', 'date', '$http', 'urls', function($scope, $rootScope, $routeParams, $location, post, date, $http, urls) {
      var getData, getStorage, postInout;
      if (!$rootScope.username) {
        return $location.path("general_login");
      } else if (!$rootScope.data_loaded) {
        return $location.path("general_home");
      } else {
        $scope.edit_id = 0;
        $scope.storageid = 0;
        $scope.pushedData = [];
        $scope.inout = "";
        $scope.payment_type = "";
        $scope.cancelData = function(stop) {
          $scope.amount = "";
          $scope.agent = "";
          $scope.partner = "";
          $scope.date = date.thisdate();
          $scope.bank = "";
          $scope.payment_code = "";
          $scope.comment = "";
          $scope.amount_error = false;
          $scope.date_error = false;
          $scope.payment_code_error = false;
          $scope.comment_error = false;
          $scope.storageid = 0;
          $scope.depot_type = "";
          $scope.cost_type = "";
          $scope.quantity = "";
          $scope.user = $rootScope.userrealname;
          if (!stop) {
            $scope.edit_id = $routeParams.id;
            return getData();
          } else {
            return $scope.edit_id = 0;
          }
        };
        getData = function() {
          var data;
          if ($scope.edit_id !== "0") {
            data = {
              method: "get",
              from: "input",
              where: "`Id` = " + $scope.edit_id,
              describe: ["from", "where"]
            };
            return post.load(data, true).then(function(data) {
              var edit_data;
              edit_data = data.data[0];
              $scope.amount = parseInt(edit_data.amount);
              $scope.inout = parseInt(edit_data.inout);
              $scope.inout_name = $rootScope.inout_list[edit_data.inout];
              $scope.partner = edit_data.partner;
              $scope.date = edit_data.date;
              $scope.payment_type = edit_data.payment_type;
              $scope.payment_type_name = $rootScope.payment_type_list[edit_data.payment_type];
              $scope.bank = parseInt(edit_data.bank);
              $scope.payment_code = edit_data.payment_code;
              $scope.comment = edit_data.comment;
              $scope.user = edit_data.user;
              $scope.storageid = edit_data.storageid;
              return getStorage();
            });
          } else {
            if ($rootScope.depot_type > 0) {
              $scope.inout = 2;
              $scope.inout_name = $rootScope.inout_list[2];
              $scope.depot_type = $rootScope.depot_type;
              $scope.cost_type = $rootScope.depot_type_byid[$scope.depot_type].cost_type;
              $rootScope.depot_type = 0;
            }
            if ($rootScope.partner > 0) {
              $scope.partner = $rootScope.partner;
              $rootScope.partner = null;
            }
            if ($rootScope.inout > 0) {
              $scope.inout = $rootScope.inout;
              $rootScope.inout = 0;
              return $scope.inout_name = $rootScope.inout_list[$scope.inout];
            }
          }
        };
        getStorage = function() {
          var data;
          if ($scope.storageid !== "0") {
            data = {
              method: "get",
              from: "depot",
              where: "`Id` = " + $scope.storageid,
              describe: ["from", "where"]
            };
            return post.load(data, true).then(function(data) {
              var edit_data;
              edit_data = data.data[0];
              $scope.depot_type = edit_data.depot_type;
              $scope.cost_type = $rootScope.depot_type_byid[$scope.depot_type].cost_type;
              return $scope.quantity = parseInt(edit_data.quantity);
            });
          }
        };
        $scope.postData = function() {
          var postData;
          if ($scope.inout > 1 && $scope.quantity > 0 && $scope.depot_type > 0) {
            postData = {
              method: "post",
              from: "depot",
              describe: ["id", "depot_type", "quantity", "inout", "user", "date", "amount", "inputid", "comment"],
              id: $scope.storageid,
              depot_type: $scope.depot_type,
              quantity: $scope.quantity,
              date: $scope.date,
              inout: 0,
              user: $rootScope.userrealname,
              date: $scope.date,
              amount: $scope.amount,
              inputid: $scope.edit_id,
              comment: $scope.comment
            };
            if ($scope["delete"] === true) {
              postData["delete"] = true;
              postData.describe.push("delete");
            }
            return post.load(postData, true).then(function(data) {
              $scope.storageid = data.data;
              postInout();
              return $http.get(urls.depot).success(function(data) {});
            });
          } else {
            return postInout();
          }
        };
        postInout = function() {
          var postData;
          postData = {
            method: "post",
            from: "input",
            describe: ["id", "amount", "inout", "partner", "date", "payment_type", "bank", "payment_code", "comment", "user", "storageid"],
            id: $scope.edit_id,
            amount: $scope.amount,
            inout: $scope.inout,
            partner: $scope.partner,
            date: $scope.date,
            payment_type: $scope.payment_type,
            bank: $scope.bank,
            payment_code: $scope.payment_code,
            comment: $scope.comment,
            user: $scope.user,
            storageid: $scope.storageid
          };
          if ($scope["delete"] === true) {
            postData["delete"] = true;
            postData.describe.push("delete");
          }
          return post.load(postData, true).then(function(data) {
            $scope.pushedData.push(postData);
            return $scope.cancelData(true);
          });
        };
        $scope.add1000 = function() {
          $scope.amount *= 1000;
          return document.getElementById("amount").focus();
        };
        $scope.add1000000 = function() {
          $scope.amount *= 1000000;
          return document.getElementById("amount").focus();
        };
        $scope.changCosttype = function() {
          return $scope.cost_type = $scope.partner_byid[$scope.partner].cost_type;
        };
        $scope.deleteData = function() {
          $scope["delete"] = true;
          return $scope.postData();
        };
        $scope.open = function($event) {
          $event.preventDefault();
          $event.stopPropagation();
          return $scope.opened = true;
        };
        $scope.dateOptions = {
          'year-format': "'yy'",
          'starting-day': 1
        };
        return $scope.cancelData();
      }
    }
  ]);

}).call(this);
