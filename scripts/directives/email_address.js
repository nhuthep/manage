(function() {
  app.directive('emailAddress', function(validation) {
    return {
      restrict: 'A',
      link: function(scope, elem, attr) {
        return scope.$watch(attr.ngModel, function(v) {
          if (!scope.submited || validation.email(v)) {
            return scope.error.email_address = false;
          } else {
            return scope.error.email_address = true;
          }
        });
      }
    };
  });

}).call(this);
