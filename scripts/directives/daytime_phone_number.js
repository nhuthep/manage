(function() {
  app.directive('daytimePhoneNumber', function(validation) {
    return {
      restrict: 'A',
      link: function(scope, elem, attr) {
        return scope.$watch(attr.ngModel, function(v) {
          if (!scope.submited || validation.mobile(v)) {
            return scope.error.daytime_phone_number = false;
          } else {
            return scope.error.daytime_phone_number = true;
          }
        });
      }
    };
  });

}).call(this);
