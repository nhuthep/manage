(function() {
  app.directive('postcode', function(validation) {
    return {
      restrict: 'A',
      link: function(scope, elem, attr) {
        return scope.$watch(attr.ngModel, function(v) {
          if (!scope.submited || validation.postcode(v)) {
            return scope.error.postcode = false;
          } else {
            return scope.error.postcode = true;
          }
        });
      }
    };
  });

}).call(this);
