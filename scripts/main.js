(function() {
  window.app = angular.module('app', ['ngRoute', 'ngCookies', 'ngAnimate', 'ui.bootstrap']);

  app.run([
    '$rootScope', '$http', 'urls', 'config', 'login', 'logout', function($rootScope, $http, urls, config, login, logout) {
      login.login();
      $rootScope.logout = function() {
        return logout.logout();
      };
      $rootScope.post_status = "Loading ...";
      return $http({
        url: 'data/config.json',
        method: "GET"
      }).success(function(data, status, headers, config) {
        var k;
        for (k in data) {
          $rootScope[k] = data[k];
        }
        return $http({
          url: urls.basicdata,
          method: "GET",
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).success(function(data, status, headers, config) {
          var attributes, item, _i, _j, _k, _l, _len, _len1, _len2, _len3, _len4, _len5, _len6, _len7, _len8, _len9, _m, _n, _o, _p, _q, _r, _ref, _ref1, _ref2, _ref3, _ref4, _ref5, _ref6, _ref7, _ref8, _ref9;
          for (attributes in data) {
            $rootScope[attributes] = data[attributes];
          }
          $rootScope.city_byid = [];
          _ref = $rootScope.city_list;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            item = _ref[_i];
            $rootScope.city_byid[item.Id] = item;
          }
          $rootScope.partner_type_byid = [];
          _ref1 = $rootScope.partner_type_list;
          for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
            item = _ref1[_j];
            $rootScope.partner_type_byid[item.Id] = item;
          }
          $rootScope.bank_byid = [];
          _ref2 = $rootScope.bank_list;
          for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
            item = _ref2[_k];
            $rootScope.bank_byid[item.Id] = item;
          }
          $rootScope.unit_byid = [];
          _ref3 = $rootScope.unit_list;
          for (_l = 0, _len3 = _ref3.length; _l < _len3; _l++) {
            item = _ref3[_l];
            $rootScope.unit_byid[item.Id] = item;
          }
          $rootScope.cost_type_byid = [];
          _ref4 = $rootScope.cost_type_list;
          for (_m = 0, _len4 = _ref4.length; _m < _len4; _m++) {
            item = _ref4[_m];
            $rootScope.cost_type_byid[item.Id] = item;
          }
          $rootScope.share_type_byid = [];
          _ref5 = $rootScope.share_type_list;
          for (_n = 0, _len5 = _ref5.length; _n < _len5; _n++) {
            item = _ref5[_n];
            $rootScope.share_type_byid[item.Id] = item;
          }
          $rootScope.card_byid = [];
          _ref6 = $rootScope.card_list;
          for (_o = 0, _len6 = _ref6.length; _o < _len6; _o++) {
            item = _ref6[_o];
            $rootScope.card_byid[item.Id] = item;
          }
          $rootScope.staff_byid = [];
          _ref7 = $rootScope.staff_list;
          for (_p = 0, _len7 = _ref7.length; _p < _len7; _p++) {
            item = _ref7[_p];
            item.gender_name = $rootScope.gender_list[item.gender];
            $rootScope.staff_byid[item.Id] = item;
          }
          $rootScope.partner_byid = [];
          $rootScope.agent_list = [];
          $rootScope.agent_byid = [];
          _ref8 = $rootScope.partner_list;
          for (_q = 0, _len8 = _ref8.length; _q < _len8; _q++) {
            item = _ref8[_q];
            item.location_name = "";
            item.partner_type_name = "";
            item.cost_type_name = "";
            if ($rootScope.city_byid[item.location]) {
              if ($rootScope.city_byid[item.location].name) {
                item.location_name = $rootScope.city_byid[item.location].name;
              }
            }
            if ($rootScope.partner_type_byid[item.type]) {
              if ($rootScope.partner_type_byid[item.type].name) {
                item.partner_type_name = $rootScope.partner_type_byid[item.type].name;
              }
            }
            if ($rootScope.cost_type_byid[item.cost_type]) {
              if ($rootScope.cost_type_byid[item.cost_type].name) {
                item.cost_type_name = $rootScope.cost_type_byid[item.cost_type].name;
              }
            }
            item.name = item.manager + ' (' + item.location_name + ')';
            $rootScope.partner_byid[item.Id] = item;
            if (item.type === "1") {
              $rootScope.agent_list.push(item);
              $rootScope.agent_byid[item.Id] = item;
            }
          }
          $rootScope.depot_type_byid = [];
          _ref9 = $rootScope.depot_type_list;
          for (_r = 0, _len9 = _ref9.length; _r < _len9; _r++) {
            item = _ref9[_r];
            item.total = 0;
            item.cost_type_name = "";
            item.unit_name = "";
            if ($rootScope.cost_type_byid[item.cost_type]) {
              if ($rootScope.cost_type_byid[item.cost_type].name) {
                item.cost_type_name = $rootScope.cost_type_byid[item.cost_type].name;
              }
            }
            if ($rootScope.unit_byid[item.unit]) {
              if ($rootScope.unit_byid[item.unit].name) {
                item.unit_name = $rootScope.unit_byid[item.unit].name;
              }
            }
            $rootScope.depot_type_byid[item.Id] = item;
          }
          $rootScope.product_byid = [];
          var p_ref = $rootScope.product_list;
          var p_i, p_len;
          for (p_i = 0, p_len = p_ref.length; p_i < p_len; p_i++) {
              item = p_ref[p_i];
              if (item.Id) {
                  $rootScope.product_byid[item.Id] = item;
              }
          }
          if ($rootScope.product_byid.length == 0) {
              $rootScope.showAddProduct = 1;
          } else {
              $rootScope.showAddProduct = 0;
          }

          return $rootScope.data_loaded = true;
        });
      });
    }
  ]);

}).call(this);
